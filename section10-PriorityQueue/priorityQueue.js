// 因为JS中没有优先队列 所以在这里实现一个
class PriorityQueue  {
    constructor(compare) {
      if(typeof compare !== 'function') {
        throw new Error('compare is not a function')
      }
      this.data = []
      this.compare = compare
    }
    // 返回的是找的的target的下标
    search(target) {
      //eg: 45 67 78 
      // low = 45 high = 78
      let low = 0, high = this.data.length
      while(low < high) {
        // >>是位运算的右移，就是把操作数对应的二进制数整体右移一位（最高位补0，最低位砍掉）。
        // >>x 相当于除以2的x次方
        // >>0 就是Math.floor() 向上取整
        // 
        let mid = (left + right) >> 1;
        // let mid = low + ((high-low) >> 1) // 0
        //[31] mid = 0
        // (a, b) => a - b  a是32 b是12  this.compare(31, 12) = 19
        // this.compare(31, 12) = 19 > 0
        if (this.compare(this.data[mid], target) > 0 ) {
            // high = 0
            high = mid
        } else {
            low = mid + 1
        }
      }
      return low;
    }
    push(elem) {
      let index = this.search(elem);
      this.data.splice(index, 0, elem);
      return this.data.length
    }
    pop() {
      return this.data.pop()
    }
    peek() {
        return this.data[this.data.length -1];
    }
}
//每次取出的是整个队列中的最小值
/*
|       |
|       |
|       |
|       |
|       |
|       |
|       |
|  31   |
*/
let qeen = new PriorityQueue((a, b) => a - b)
qeen.push(31)
qeen.push(12)
qeen.push(2)
console.log(qeen.data)