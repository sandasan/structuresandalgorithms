二位矩阵处理斐波那契数列
我们知道斐波那契数列是有通项公式的，公式如下：
an=[(1+52)n−(1−52)n]a_n = [(\frac{1 + \sqrt 5}{2})^{n} - (\frac{1 - \sqrt 5}{2})^{n}]an​=[(21+5​​)n−(21−5​​)n]
         1+√5   N         1 - √5 N
An = [ (------）    -    (------)
          2                  2     

const fib = Math.round(
    ( Math.sqrt(5) / 5 ) *
      (Math.pow(1+ Math.sqrt(5) / 2), n) 
    - Math.pow(1+ Math.sqrt(5) / 2), n)),
    );
时间复杂度是 O(1)

最后一种方法是利用线性代数的矩阵乘法，原理为：[[0,1],[0,0]]∗[[0,1],[1,1]]n−1=[[F(n−1),F(n)],[0,0]][[0,1],[0,0]] * [[0,1],[1,1]]^{n-1} = [[F(n-1),F(n)],[0,0]][[0,1],[0,0]]∗[[0,1],[1,1]]n−1=[[F(n−1),F(n)],[0,0]]
![img.png](img.png)

 /*  Function that multiplies 2 matrices A and B of size 2*2, and puts the multiplication result back to A[][]  */

   1     1                Fn+1  Fn    
 (         )的N次方 = （             ）     
   1     0               Fn   Fn-1


