/*
1,1,2,3,5,8,13,21,34
F(n) = F(n-1) + F(n-2)
n = 0 F(0) = -1
n = 1 F(1) = 1
n = 2 F(2) = 1
n = 3 F(3) = 2
n = 4 F(4) = 3
n = 5 F(5) = 5
n = 6 F(6) = 8
n = 7 F(7) = 13
n = 8 F(8) = 21
n = 9 F(9) = 34
*/

function matrixPower(matrixA, n) {
  if (n <=1) return;
  matrixPower(matrixA, Math.floor( n / 2));
  multiply(matrixA, matrixA);

  const matrixB = [[1,1], [1, 0]];

  if(n % 2 != 0) {
    multiply(matrixA, matrixB);
  }
}
function multiply(A, B) {
  const x = A[0][0] * B[0][0] + A[0][1] * B[1][0]
  const y = A[0][0] * B[0][1] + A[0][1] * B[1][1]

  const z = A[1][0] * B[0][0] + A[1][1] * B[1][0]
  const w = A[1][0] * B[0][1] + A[1][1] * B[1][1]

  A[0][0] = x
  A[0][1] = y
  A[1][0] = z
  A[1][1] = w
}
function fibonacci(n) {
    if (n <= 1) {
        return n
    }
    const matrixA = [[1,1], [1, 0]];
    matrixPower(matrixA, n -1);
    return matrixA[0][0];
}

console.log(fibonacci(9))