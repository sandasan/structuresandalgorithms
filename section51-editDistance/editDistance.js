function misDistance(s1, s2) {
    const lent1 = s1.length;
    const lent2 = s2.length;
    let matrix = [];
    for (let i = 0;  i <= lent1; i++ ) {
        matrix[i] = new Array();
        for(let j= 0; j < lent2; j++) {
          if (i == 0) {
             matrix[i][j] = j
          } else if (j ==0) {
            matrix[i][j] = i
          } else {
            // 进行最小值分析
            let cost = 0;
            // 相同为0，不同置1
            if (s1[i-1] != s2[j-1]) {
              cost =1
            }
            matrix[i][j] = Math.min(matrix[i-1][j] + 1, matrix[i][j - 1] + 1, matrix[i-1][j-1] + cost)
          }
        }
    }
    return matrix[lent1][lent2]

}

console.log(minDistance('jary','jerry'))
