给你两个单词 word1 和 word2， 请返回将 word1 转换成 word2 所使用的最少操作数  。

你可以对一个单词进行如下三种操作：

插入一个字符
删除一个字符
替换一个字符

来源：力扣（LeetCode）
链接：https://leetcode.cn/problems/edit-distance
著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。


输入：word1 = "horse", word2 = "ros"
输出：3
解释：
horse -> rorse (将 'h' 替换为 'r')
rorse -> rose (删除 'r')
rose -> ros (删除 'e')


输入：word1 = "intention", word2 = "execution"
输出：5
解释：
intention -> inention (删除 't')
inention -> enention (将 'i' 替换为 'e')
enention -> exention (将 'n' 替换为 'x')
exention -> exection (将 'n' 替换为 'c')
exection -> execution (插入 'u')

编辑无非就是三种情况，字符的插入、删除以及编辑：

插入一个字符为进行了一次操作，如：fat->fait;

删除一个字符也视为进行一次操作，如：haven->have;

替换字符也视为进行一次操作，如：let->lit。

这个算法的原理不太好理解，但放到矩阵中实现的过程很简单，我们把两个字符串放到矩阵里进行解析，这里用到了动态规划：

在相同位置上两个字符串不同:cost=1；反则为0

matrix[m][n]=Math.min(matrix[m-1][n]+1,matrix[m][n-1]+1,matrix[m-1][n-1]+cost)
其中matrix[m-1][n]+1代表删除操作，matrix[m][n-1]+1代表新增，matrix[m-1][n-1]+cost代表字符的替换，然后求出他们三个值的最小值。


作者：mytac
链接：https://www.jianshu.com/p/90af98493661
来源：简书
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。

1.矩阵初始化
先构建一个首行首列都从0增长的矩阵，长度为(s1.length+1)*(s2.length+1)。
     i  v  a  n  1
  0  1  2  3  4  5
i 1
v 2
a 3
n 4
2 5

2.计算最小值
之后开始对比两个字符串的每个位置，按照上一节的公式取最小值。
matrix[m][n]= Math.min(matrix[m-1][n]+1,matrix[m][n-1]+1,matrix[m-1][n-1]+cost)
其中matrix[m-1][n]+1代表删除操作，matrix[m][n-1]+1代表新增，matrix[m-1][n-1]+cost代表字符的替换.


      i    v    a    n    1
  0   1    2    3    4    5
i 1
v 2
a 3
n 4
2 5
