class NodeList {
    constructor(val) {
        this.val = val;
        this.next = null;
    }
    append(val) {
      let current = this;
      let node = new NodeList(val)
      while (current.next) {
          current = current.next
      }
      current.next = node
    }
    print() {
        let current = this;
        let res = []
        while(current) {
            res.push(current.val);
            current = current.next
        }
        console.log(res)
    }
}

let node1 = new NodeList(3);
node1.append(2)
node1.append(0)
node1.append(-4)
node1.append(1)
node1.print()

var hasCycle = function(head) {
    if (head == null || head.next == null ) return false;
    let fast = head.next.next,
        slow = head.next
    while(fast !== slow) {
        if(!fast || !fast.next) return false
        fast = fast.next.next
        slow = slow.next
    }
    return true
}

console.log(hasCycle(node1))
