class NodeList {
    constructor(val) {
        this.val = val;
        this.next = null;
   }
   append(val) {
     let current = this;
     let node = new NodeList(val);
     while(current.next) {
        current  = current.next   
     }
     current.next = node;
   }

   print() {
    let current = this;
    let res = [];
    while(current.next) {
        res.push(current.val);
        current = current.next 
    }
    console.log(res);
   }
}

let node = new NodeList(2);
// { val: 2, next: NodeList { val: 45, next: null } }
node.append(45);
node.append(67);
node.append(68);
node.append(618);
node.append(45);
console.log(node);

const hasCycle = function(head) {
  if (!head || !head.next) return false;
  let fast = head.next.next;
  let slow = head.next;
   while(fast !== slow) {
     if (!fast || !fast.next) return false;
     fast = fast.next.next;
     slow = slow.next;
   }
   return true;
}

console.log(hasCycle(node));


