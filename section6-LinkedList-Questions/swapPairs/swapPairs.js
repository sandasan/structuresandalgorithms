// 链表交换相邻元素

class NodeList {
   constructor(val) {
     this.val = val
     this.next = null;
   } 

   append(val) {
     let current = this
     let node = new NodeList(val);
     while(current.next) {
        current = current.next
     }
     current.next = node
   }

   print() {
      let current = this
      let res = []
      while(current){
        res.push(current.val)
        current = current.next
      }
      console.log(res);
   }
}

let node1 = new NodeList(1);
node1.append(2)
node1.append(3)
node1.append(4)
node1.append(5)
node1.append(6)

var swapPairs = function(head) {
 // build a dummy nodes link to head, like [-1,1,2,3,4]
  let dummy = new NodeList(-1);
  dummy.next = head;
  // assign dummy to prev because prev will change to  forward nodes, 
  // but dummy always link to first node,
  // in this example is link to [2...]
  let pre = dummy;
  // loop when head and head.next is not null
  while(head && head.next) {
    // cur node 
    let n1 = head;
    // next node
    let n2 = head.next;
    // 申请完变量后，首先将前置节点指向n2，因为交换完成后n2将是第一个节点
    pre.next = n2;
    // 将n1节点指向n2的后一个节点，第一次情况下也就是[3,4]
    n1.next = n2.next;
    // 然后第一次交换相邻节点，将n2节点指向n1节点
    n2.next = n1;
    // 第一次交换完成后，链表结构变成[-1, 2, 1, 3, 4]这样
    // 在迭代的最后，将前置节点prev改变为n1，并将当前节点head改变为n1.next，为第二次迭代做准备
    pre = n1;
    head = n1.next;
  }

  return dummy.next
}

var swapPairs = function(head){
  // stop
  if(!head || !head.next) return head 
   // do
   // 然后，我们申请3个变量，v1为当前节点，v2为下一个节点，v3为第三个节点
  let data1Pointer = head // 为当前节点
  let data2Pointer = data1Pointer.next // 下一个节点

  let v3 = data2Pointer.next // 第三个节点

  //我们交换v1和v2，继续递归v3并赋给v1
  data2Pointer.next = data1Pointer
  // recursion
  data1Pointer.next = swapPairs(v3)
  
  // 我们返回v2， 因为无论是那次递归，v2都将代表本次交换中的第一个节点
  return data2Pointer
}

swapPairs(node1).print()
// https://juejin.cn/post/7074885077011791909