 删除链表的倒数第 N 个结点
 https://leetcode.cn/problems/remove-nth-node-from-end-of-list/description/
// 给定一个链表
input
[3,5,6,7,8,9] 删除倒数第2位 pos = 2
output
[3,5,6,7,9]
/*
 * @lc app=leetcode.cn id=19 lang=javascript
 *
 * [19] 删除链表的倒数第N个节点
 *
 * https://leetcode-cn.com/problems/remove-nth-node-from-end-of-list/description/
 *
 * algorithms
 * Medium (40.49%)
 * Likes:    1132
 * Dislikes: 0
 * Total Accepted:    288.4K
 * Total Submissions: 710.9K
 * Testcase Example:  '[1,2,3,4,5]\n2'
 *
 * 给定一个链表，删除链表的倒数第 n 个节点，并且返回链表的头结点。
 * 
 * 示例：
 * 
 * 给定一个链表: 1->2->3->4->5, 和 n = 2.
 * 
 * 当删除了倒数第二个节点后，链表变为 1->2->3->5.
 * 
 * 
 * 说明：
 * 
 * 给定的 n 保证是有效的。
 * 
 * 进阶：
 * 
 * 你能尝试使用一趟扫描实现吗？
 * 
 */
 删除从列表开头数起的第 L - N + 1个结点，其中 
 是列表的长度。只要我们找到列表的长度 
，这个问题就很容易解决
 长度是5  n = 2
 11 12 13 14 15
 删除的是14  是正数的第4个元素 5 - 2 + 1 = 4

 1. 建立一个空的哑结点作为辅助
 2. 让哑结点指向Head
 3. 循环遍历找到链表的长度 L （定义一个长度，然后让 first指针指向head， 判定first不是null 让 length ++ ， first指针指向下一个节点）