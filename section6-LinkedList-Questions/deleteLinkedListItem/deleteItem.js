class NodeList {
    constructor (val) {
       this.val = val;
       this.next = null;
    }
    append (val) {
      let current = this;
      let node = new NodeList(val);
      while(current.next) {
        current = current.next
      }
      current.next = node;
    }
    print() {
        let current = this;
        let res = [];
        while(current) {
         res.push(current.val)
         current = current.next 
        }
        console.log(res)
    }
}

let node1 = new NodeList(1);
node1.append(2)
node1.append(3)
node1.append(4)
node1.append(5)

/*
时间复杂度：
，
该算法对列表进行了两次遍历，首先计算了列表的长度 L
其次找到第 L- N个结点。 操作执行了 2L-N 步，时间复杂度为 O (L)
空间复杂度 O(1)：我们只用了常量级的额外空间。
*/
var romoveNthFromEnd =  (head, n) => {
     let dummy = new NodeList(0);
     dummy.next = head;
     // 计算链表的长度
     let length = 0;
     let first = head;
     while(first !== null){
        length++;
        first = first.next;
     }
     // 设置一个指向哑结点的指针，并移动它遍历列表，直至它到达第L-N个结点那里
    length = length - n;
    first = dummy;
    while(length > 0) {
        length--;
        first = first.next
    }
    // 我们把第 L- N 个结点的 next 指针重新链接至第 L- N + 2 个结点，完成这个算法。
    first.next = first.next.next;
    return dummy.next;

}
romoveNthFromEnd(node1, 2).print()







