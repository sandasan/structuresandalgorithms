class NodeList {
    constructor (val) {
       this.val = val;
       this.next = null;
    }
    append (val) {
      let current = this;
      let node = new NodeList(val);
      while(current.next) {
        current = current.next
      }
      current.next = node;
    }
    print() {
        let current = this;
        let res = [];
        while(current) {
         res.push(current.val)
         current = current.next 
        }
        console.log(res)
    }
}

let node1 = new NodeList(10);
node1.append(23)
node1.append(34)
node1.append(45)
node1.append(56)
node1.append(59)
node1.append(89)
node1.print()
/*
 上述算法可以优化为只使用一次遍历。我们可以使用两个指针而不是一个指针。
 第一个指针从列表的开头向前移动N + 1步，而第二个指针将从列表的开头出发。
 现在，这两个指针被N结点分开。我们通过同时移动两个指针向前来保持这个恒定的间隔，
 直到第一个指针到达最后一个结点。
 此时第二个指针将指向从最后一个结点数起的第N个结点。
 我们重新链接第二个指针所引用的结点的 next 指针指向该结点的下下个结点。 


   D    1   2  3  4  5  NULL
  (s)  (HEAD)
  (F)
  第一次移动
  第一个指针从列表的开头向前移动 N + 1 步 (2+1 =3 )
   D     1     2    3    4    5    NULL
  (s)  (HEAD)      (F)
  第二次移动 F指向NULL
   D     1     2    3    4    5    NULL
      (HEAD)       (s)             (F)
  
  第三次移动 发现第一个指针指向了空所以，此时第二个指针将指向从最后一个结点数起的第N个结点
   D     1     2    3    4    5    NULL
       (HEAD)      (s)  （X）       (F)    

  此时重新链接第二个指针所引用的结点的next指针指向该结点的下下个结点。      
   D     1     2    3     5    NULL
       (HEAD)      (s)          (F) 
    
 */

 var removeNthFromEnd = function (head, n){
   let dummy = new NodeList(0);
   dummy.next = head;
   let first = dummy;
   let second = dummy;
  // Advances first pointer so that the gap between first and second is n nodes apart
   for (let i = 1; i <= n + 1; i ++ ) {
     first = first.next;
  }
  // Move first to the end, maintaining the gap
   while (first != null) {
      first = first.next;
      second = second.next;
   }
   second.next = second.next.next;
   return dummy.next;
 }

 removeNthFromEnd(node1, 7).print()

