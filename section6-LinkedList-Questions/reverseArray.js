// reverse
let arr = [1, 2, 3, 4, 5]
//console.log(arr.reverse())

// 头尾交换
/*
  数组长度是3
  头  尾
  1 2 3 
  头是1, 尾是3, 2的位置不换
  交换之后
  3 2 1
  交换了 3/2+1 = 1 次


  数组长度是4
  头    尾
  1 2 3 4
  头是1, 尾是4, 2,3的位置不换
  第一轮交换之后
  4 2 3 1

  此时 头是4，尾是1，对2 3进行头尾交换
  第二轮交换之后
  4 3 2 1

  交换了 4/2+1 ～= 2次


  数组长度是5
  头      尾
  1 2 3 4 5
  头是1, 尾是5, 2,3,4的位置不换
  第一轮交换之后
  5 2 3 4 1
  第二轮交换之后
  5 4 3 2 1

  交换了 5/2+1 ～= 2次


  数组长度是6
  头        尾
  1 2 3 4 5 6
  第一轮交换之后
  6 2 3 4 5 1
  第二轮交换之后
  6 5 3 4 2 1
  第三轮交换之后
  6 5 4 3 2 1

  交换了 6/2+1 = 2 次

  */
  let array = [1, 2, 3]
  function reverseArray(array) {
    // Math.floor向上取整
    for(let i = 0; i < Math.floor(array.length / 2); i ++ ) {
        [array[i], array[array.length - i - 1]] = [array[array.length - i - 1], array[i]]
      }
      return array;
  }
  
  console.log(reverseArray(array))