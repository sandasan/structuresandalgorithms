class NodeList {
    constructor(val) {
      this.val = val;
      this.next = null;
    }
    append(val) {
       let current = this;
       let node = new NodeList(val);
       while(current.next) {
        current = current.next
       }
       current.next = node
    }
    print () {
        let current = this;
        let res = [];
        while(current) {
            res.push(current.val)
            current = current.next
        }
        console.log(res)
    }
}

let node1 = new NodeList(1);
node1.append(2)
node1.append(3)
node1.append(4)
node1.append(5)
node1.append(6)
node1.append(6)

var deleteDuplicates = function(head) {
   let current = head;
   while(current && current.next) {
      if (current.val === current.next.val) {
        current.next = current.next.next;
      } else {
        current = current.next
      }
   }
   return head;
}
console.log('去重前')
node1.print();
console.log('去重后')
deleteDuplicates(node1).print()
