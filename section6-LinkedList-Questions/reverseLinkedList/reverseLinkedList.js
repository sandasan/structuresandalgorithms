// reverse
// 什么是链表？一个长度为n，无法通过下标来遍历，只能通过当前节点来访问下一个节点的链式结构
// 链表只能由当前节点访问下一个节点，无法通过下标来访问链表元素
/*
  1 2 3 4
  采用局部反转  先反转2位 1 2 3 4 变成 2 1 3 4
  2 1 3 4
  再采用局部反转 反转3位 2 1 3 4  变成 3 2 1 4
  3 2 1 4
  最后采用局部反转 反转4位 3 2 1 4 变成 4 3 2 1
  4 3 2 1
*/

// 构造一个简单的链表
class NodeList {
  // 利用构造函数定义val值和它的指针的next
   constructor(val) {
     this.val = val
     this.next =null
   }
   // 添加的方法  参数val
   append(val) {
     let current = this
     let node = new NodeList(val);
     while(current.next) {
       current = current.next
     }
     current.next = node
   }
   // 打印的方法
   print() {
     let current = this
     let res = []
     while(current) {
       res.push(current.val)
       current = current.next
     }
     console.log(res)
   }
}

let node1 = new NodeList(1)

// console.log(node1)  { val: 1, next: null }
node1.append(2)

 // console.log(node1) { val: 1, next: NodeList { val: 2, next: null } }
 //node1.append(3)

/*
console.log(node1)
 NodeList {
  val: 1,
  next: NodeList { val: 2, next: NodeList { val: 3, next: null } }
} 
*/
//node1.append(4)
/*
console.log(node1)
NodeList {
  val: 1,
  next: NodeList { val: 2, next: NodeList { val: 3, next: [NodeList] } }
}
*/
//node1.append(5)
/*
console.log(node1)
NodeList {
  val: 1,
  next: NodeList { val: 2, next: NodeList { val: 3, next: [NodeList] } }
}
*/

/*
 head: 
 NodeList {
  val: 1,
  next: NodeList { val: 2, next: NodeList { val: 3, next: null } }
} 
*/
const f = (head) => {
  // 记录当前指针cur
  let cur = head
  // 记录前一个节点的指针pre
  let pre = null;
  // cur: { val: 1, next: NodeList { val: 2, next: null } }
  while(cur) {
     // 把curr.next 赋值给nextTemp变量，即nextTemp 指向curr的下一节点（即节点2）
    const nextTemp = cur.next //cur.next -> NodeList { val: 2, next: null }
    // 把prev赋值给curr.next,因为prev初始化化指向null，即curr(节点1)指向了null，
    cur.next = pre // cur.next -> NodeList { val: 2, next: null } pre-> null
    // 把curr赋值给prev，prev指向curr
    pre = cur
    cur = nextTemp
  }
  return pre
}
console.log('反转前')
node1.print()
console.log('反转后')
f(node1).print()
// https://juejin.cn/post/6844904058562543623
