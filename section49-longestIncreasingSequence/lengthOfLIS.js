/*
贪心 + 二分查找
关于贪心和二分查找还不了解的同学们可以移步我的这两篇专栏入个门。

「算法思想」分治、动态规划、回溯、贪心一锅炖
从酒桌游戏看二分查找算法
这里再结合本题理解一下贪心思想，同样是长度为 2 的序列，[1,2] 一定比 [1,4] 好，因为它更有潜力。换句话说，我们想要组成最长的递增子序列，就要让这个子序列中上升的尽可能的慢，这样才能更长。
我们可以创建一个 tails 数组，用来保存最长递增子序列，如果当前遍历的 nums[i] 大于 tails 的最后一个元素(也就是 tails 中的最大值)时，我们将其追加到后面即可。否则的话，我们就查找 tails 中第一个大于 nums[i] 的数并替换它。因为是单调递增的序列，我们可以使用二分查找，将时间复杂度降低到 O(logn) 。
*/
const lengthOFLIS =function(nums) {
  let len = nums.length;
  if(len < 1) {
    return len
  }
 // 数组尾巴
 let tails = [nums[0]]
 for(let i = 0; i < len; i++) {
   // 当前遍历元素 nums[i] 大于 前一个递增子序列的 尾元素时，追加到后面即可
  if(nums[i] > tails[tails.length - 1 ]) {
    tails.push(nums[i])
  } 
 // 否则，查找递增子序列中第一个大于当前值的元素，用当前遍历元素 nums[i] 替换它
 // 递增序列，可以使用二分查找
  else {
    let left = 0;
    let right = tails.length - 1;
    while(left < right) {
        let mid = (left + right) >> 1
        // 找到的话直接返回
        if (tails[mid] === nums[i]) {
          return tails.length;
        }
        // 若目标数nums[i]大于中间的数因为单调递增的所以只能去数组右边继续查找
        // 将左界变成mid+1
        else if (tails[mid] < nums[i]) {
            left = mid + 1;
        } else {
            right = mid;
        }
    }
    tails[left] = nums[i]
  }
 }
 return tails.length;
 
}

console.log(lengthOFLIS([10, 9, 2, 5, 3, 7, 101, 18, 20]))










