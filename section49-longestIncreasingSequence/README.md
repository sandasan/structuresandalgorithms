最长上升子序列
10 9 2 5 3 7 101 18 20

2 3 7 18 20

如何解决?
子序列必须连续
1. 暴力求解
2. DP

状态
从头元素到第i个元素，要选上i的最长子序列的长度
DP[i]
状态转移方程

for i 0 ---> n - 1
  
DP[i] = MAX { DP[j] + 1 } 第i个元素要选进来所以加1
j取值范围 0 -> i - 1 && a[j] < a[i] j 必须是i的前序数

for i 0 --> n-1
 for j 0 --> i-1
所以是O(N^2)的时间复杂度
如何可以用O（N * logN）
3. 二分查找方案
因为是O(N^2)是因为有2层循环
for i 0 --> n-1
 for j 0 --> i-1
这里只能加速里面的循环。
for j 0 --> i-1替换成二分查找。

1. 维护一个数组是上升子序列LIS
2. for i 0 - n-1
    插入到LIS
    
10 9 2 5 3 7 101 18 20

10 进来
[10]
9 比 10 小进行替换
[9]
2进来发现比9小替换
[2]
5进来发现比2大，进来，
[2, 5]
3 进来用二分查找然后把5替换掉
[2, 3]
7 进来
[2, 3, 7]
101 进来
[2, 3, 7, 101]
18 进来二分查找
[2, 3, 7, 18]
20 进来
[2, 3, 7, 18, 20]
返回 数组的长度

https://github.com/Geekhyt/javascript-leetcode
























