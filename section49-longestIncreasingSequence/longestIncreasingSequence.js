/*
时间复杂度：O(n^2)
空间复杂度：O(n)
当我们遍历 nums[i] 时，需要同时对比已经遍历过的 nums[j]
如果 nums[i] > nums[j]，nums[i] 就可以加入到序列 nums[j] 的最后，长度就是 dp[j] + 1
注：(0 <= j < i) (nums[j] < nums[i]) 
*/
var lengthOFLIS = function(nums) {
   let n = nums.length;
   if (n==0) {
    return 0
   }
   let dp = new Array(n).fill(1)
   for (let i = 0; i < n; i ++ ) {
    for (let j=0; j < i; j++) {
        if(nums[j] < nums[i]){
            dp[i] = Math.max(dp[i], dp[j] + 1)
        }
    }
   }
   return Math.max(...dp)
}

console.log(lengthOFLIS([10, 9, 2, 5, 3, 7, 101, 18, 20]))




















