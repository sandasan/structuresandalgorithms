// 求最短路径即求每一层元素的最小值的和。双层遍历，从最底层开始遍历，直到最顶层，将每层最小值的结果相加，即可得到求解。
var minimumTotal = function(triangle) {
   let len = triangle.length;
   // 构造一个二维数组
   let dp = new Array(len+1).fill(new Array(len+1).fill(null))
   console.log('dp', dp)
   for(let i = len -1; i>=0; i--) {
    for(let j = 0; j < len; j++){
        if(i == len -1) {
            dp[i][j] = triangle[i][j]
        } else {
            dp[i][j] = Math.min(dp[i+1][j], dp[i + 1][j + 1]) + triangle[i][j]
        }
    }
   }
   return dp[0][0]
}
var triangle = [[2],[3,4],[6,5,7],[4,1,8,3]]
console.log(minimumTotal(triangle));




























