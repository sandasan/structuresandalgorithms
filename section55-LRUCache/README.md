cache 缓存
记忆
钱包-储物柜
代码模块
CPU Socket

L1 D-cache
L1 I-cache
L2 cache
L3 cache

LRU cache 缓存替换算法
1. Least recently used 最近最少使用
2. Double LinkedLink
3. O(1) 查询
4. O(1) 修改更新
![img.png](img.png)
最近使用的永远放在最前面

LFU
Least frequently used
最近最不常用的页面置换算法
LRU
Least recently used
替换算法原理

cache存在一个双向的链表里面,存在头尾，从头部进入，从尾部出来。

A进来,看cache里面是否有A，若没有加入进去，加入头部
Front 
       A




Rear

B进来，发现cahche里面没有，所以也放进去，但是要在头部
Front 
       B
       A




Rear 

C进来，发现cahche里面没有，所以也放进去，但是要在头部
Front 
       C
       B
       A

Rear 

D进来，发现cahche里面没有，所以也放进去，但是要在头部
Front 
       D
       C
       B
       A

Rear 

E进来，发现cahche里面没有，所以也放进去，但是要在头部
Front 
       E
       D
       C
       B
       A
Rear 

当F进来，发现cahche已经满了，则F放在最近的头部位置，尾部的A元素
删除了
Front 
       E
       D
       C
       B
       A
Rear 
Front  F
       E
       D
       C
       B
Rear 


当C进来，发现cahche里面有C则不需要淘汰其他元素，需要把C放置在头部

Front  C
       F
       E
       D
       B
Rear 

当G进来发现里面没有则淘汰尾部的元素B把G放置在头部

Front  G
       C
       F
       E
       D
Rear 
































