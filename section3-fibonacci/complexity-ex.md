O(1) Constant Complexity: 常数复杂度
eg:
int n = 100; 
console.log(n)

eg:
int n = 100;
console.log(n)
console.log('And more' + n)


O(n) Linear Complexity: 线性时间复杂度
一层for循环，从1到n, 需要循环N次，仅需要N次
for(int i = 1; i < n; i++) {
   console.log(n) 
}

O(n^2): N square Complexity: 平方
二层for循环，外层循环N次里层也要循环N次，是N平方
for(int i = 1; i < n; i++) {
   for(int j = 1; j < n; j++) {
   console.log(n) 
 }
} 
O(log n) Logarithmic Complexity: 对数复杂度
for(int i = 1; i < n; i = i * 2) {
   console.log(n) 
}

O(2^n): Exponential Growth 指数
for(int i = 1; i < Math.pow(2,n); i++) {
   console.log(n) 
}

O(n!): Factorial 阶乘
for(int i = 1; i < factorial(n); i++) {
   console.log(n) 
}