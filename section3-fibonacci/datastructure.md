Data Structure

Array
Stack(堆)/ Queue
PriorityQueue(heap)
LinkedList(single/double)

Tree/Binary Tree
Binary Search Tree
HashTable
Disjoint Set
Trie
BloomFilter
LRU Cache

Algorithm

General Coding
In-order/Pre-order/Post-order traversal
Greedy
Recurision/BackTrace
Breadth-first search
Depth-first search
Divide and Conquer
Dynamic Programming
Binary Search
Graph


