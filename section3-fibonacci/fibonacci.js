/*
1,1,2,3,5,8,13,21,34
F(n) = F(n-1) + F(n-2)
n = 0 F(0) = -1
n = 1 F(1) = 1
n = 2 F(2) = 1
n = 3 F(3) = 2
n = 4 F(4) = 3
n = 5 F(5) = 5
n = 6 F(6) = 8
n = 7 F(7) = 13
n = 8 F(8) = 21
n = 9 F(9) = 34
*/

// 时间复杂度 O(2^n)
function fib(n) {
    if (n <= 0 ) {
        return -1;
    } else if (n == 1 || n == 2) {
        return 1
    } else {
        return fib( n - 1 ) + fib( n - 2 )
    }
}
// 当n等于50直接卡死由于存在大量的递归导致计算过长
//console.log('fib Test', fib(8))

// 进行优化方案01  利用数组存值 时间复杂度O(n)，空间复杂度O(n) 
function fibOpt(n) {
    // 申请了一个大小为 n 的 int 类型数组，除此之外，剩下的代码都没有占用更多的空间，
    // 所以整段代码的空间复杂度就是 O(n)
    let a = [];
    if (n <= 0 ) {
        return -1;
    } else {
        // 0 1 2 3 4 5 6 7 8 9
        // 1,1,2,3,5,8,13,21,34
        // 数组的第0项和第一项是1
        a[0] = a[1] = 1
         // 数组的第2项是2
        a[2] = 2
        // 控制循环从n=3开始
        for(let i = 3;  i < n + 1; i++) {
           //console.log('for', n)
           a[i] = a[i-1] + a[i-2]
        }
    }
    // 注意这里是返回数组的index对应的数
    return a[n - 1]
}

//console.log('fibOpt Test', fibOpt(8))

// Opt 优化方案03 借助2个变量 时间复杂度O(n)，空间复杂度O(1) 
// 1,1,2,3,5,8,13,21,34
function fibOptOther(n) {
    if (Number.isInteger(n)) {
       let a, b, c
       if (n <= 0) {
         return -1
       } else {
         if (n == 1 || n == 2){
            return 1
         }
         a = b = 1
         for (let i = 3; i < n + 1; i++) {
            // 1,1,2,3,5,8,13,21,34
            console.log(`---n是${n}`, a,b)
             c = a + b
             a = b
             b = c
             
         }
         return c
       }
    }
}

//console.log(fibOptOther(5))