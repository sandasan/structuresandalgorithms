// 有效的字母异位词
const  s = "anagram", t = "nagaram"
function fun1(s,t){
    var sList = s.split("");
    var tList = t.split("");
     console.log(sList.sort())
    if (sList.sort().toString() == tList.sort().toString()) {
        return true;
    } else {
        return false;
    }
}    

console.log(fun1(s,t))

function fun2(s,t){
    var arr = new Array(26).fill(0);
        if(s.length !== t.length)
            return false;
        var aCode = 'a'.charCodeAt(); // 这样就不用背码表了
        for(var i=0; i < s.length; i++){
            arr[s.charCodeAt(i)-aCode]++;
            arr[t.charCodeAt(i)-aCode]--;
        }
        if(arr.some(item => item!==0)){
            return false;
        }
        return true;
}
console.log(fun2(s,t))
