function Node(key) {
    this.key = key;
    this.left = null;
    this.right = null;
}
   /*
           A
     B     C      D  
  E     F    
   */
let nA = new Node('A'),
nB = new Node('B')
nC = new Node('C'),
nD = new Node("D"),
nE = new Node("E")
nF = new Node("F")
nH = new Node("H")
nI = new Node("I")
nJ = new Node("J")
nK = new Node("K")
nG = new Node("G")
nL = new Node("L")

/*

                  A
                B    C
             E   F  
           K
*/
nA.left = nB
nA.right = nC;

nB.left = nE
nB.right = nF;

nE.left = nK

var maxDepth = function(root) {
    if (root === null ) {
      return 0;
    }
    // DFS 左节点算出max
    var resLeft = maxDepth(root.left)
    // DFS 右节点算出max
    var resRight = maxDepth(root.right)
    var maxLeftAndRight = Math.max(resLeft, resRight)
    return 1 + maxLeftAndRight;
 };

 console.log(maxDepth(nA))