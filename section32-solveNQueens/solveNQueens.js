class Quess {
    constructor(num) {
        // 棋盘的行或者列总数
        this.num = num;
        this.arr = []; // 棋盘设置成2维数组
        this.resust = []; // 存放放置的结果
        this.initList(); // 初始化棋盘
        this.buildList(this.arr, 0); // 建立棋盘
    }
    /*
    [
        [ 0, 0, 0, 0 ],
        [ 0, 0, 0, 0 ], 
        [ 0, 0, 0, 0 ],
        [ 0, 0, 0, 0 ]
     ]
    */
    initList() {
       let num = this.num;
       for (let i = 0; i < num; i++) {
        this.arr[i] = []
         for (let j = 0; j < num; j++) {
            this.arr[i][j] = 0;
         }
        }
    }

    buildList(list, row) {
      // 递归中止条件,找到一个解缓存起来
      if (row === list.length) {
         this.resust.push(JSON.parse(JSON.stringify(list)))
         return;
      }
      //所在行不用判断，每次都会下移一行
      //判断同一列的数据是否包含 所以在这里要循环列
      for (let col = 0, len = list.length; col < len; col++) {
        // 检测列的每一个格子是否可以放置能放置皇后则置为1
         if (this.isSafe(list, row, col)) {
            list[row][col] = 1;
            // row 行加1往下一行找进行DFS递归
            this.buildList(list, row + 1);
            // 走到这里，说明该次递归已经结束，不管找没找到，都需要重置
            list[row][col] = 0;
         }
      }
    }
    // row 行 col列
    isSafe(list, row, col) {
      const len = list.length;
      
       // eg
       /*
         [
            [(0,0), (0,1), (0,2), (0,3)],
            [(1,0), (1,1), (1,2), (1,3)],
            [(2,0), (2,1), (2,2), (2,3)],
            [(3,0), (3,1), (3,2), (3,3)]
         ]  
       */
       // 同一列 list[i][col]
       /* 
       col 相同, row行不同， 没一次递增检测
       第1列   第2列   ...
       (0,0)  (0,1)
       (1,0)  (1,1)
       (2,0)  (2,1)
       (3,0)  (3,1)
       */
      for (let row = 0; row < len; row++) {
        if (list[row][col] === 1) {
           return false;
        }
      } 
      // 斜右上放
      /*
         [            Q
            [(0,0), (0,1), (0,2), (0,3)],
            [(1,0), (1,1), (1,2), (1,3)],
            [(2,0), (2,1), (2,2), (2,3)],
            [(3,0), (3,1), (3,2), (3,3)]
         ] 
        假如Q在(0,1) 斜右上放有 (1, 2) (2,3)
       */
      for (let i = row - 1, j = col + 1; i >=0 && j < len; i--, j++) {
        if (list[i][j] === 1) {
            return false;
        }
      }
      // 斜左上方
      for (let i = row - 1, j = col -1 ; i >=0 && j>=0; i--, j-- ) {
        if (list[i][j] === 1) {
            return false;
         }
      }

      return true;
    }
}

const queen = new Quess(4);
console.log(queen.resust)