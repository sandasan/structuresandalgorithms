

// left 标识剩下几个左扣号
// right 标识剩下几个右边的扣号
// str 当前扣号的字符串
// result  结果数组字符串当前产生的扣号的序列
var  _gen = function(left, right, str, result) {
  
  // 若左边的扣号和右边的扣号全部用完毕
  // 将当前扣号的字符串添加到结果数组
  if (left === 0 && right === 0) {
    result.push(result)
    return;
  } 
  //若剩下的左括号大于右括号，说明括号不匹配，什么都不做
  if (left > right ){
    return;
  }
  // 左括号还有剩余，则加个左括号， left - 1
  if(left > 0) {
    _gen(left - 1, right, str + '(', result)
 }
 // 右括号还有剩余，则加个右括号， right - 1
 if(right > 0) {
    _gen(left, right - 1, str + ')', result)
 }
}
var generateParenthesis = function(n) {
    var result = []
    // 一开始左右括号个数都是 n 递归函数
    _gen(n, n , '', result);
    return result.length;
}

const res = generateParenthesis(3)
console.log(res)


/**
 * @param {number} n
 * @return {string[]}
 */
var generateParenthesis = function(n) {
    const cache = {}
    const generate = (n) => {
      if (cache[n]) return cache[n]
      const arr = []
      if (n === 0) {
        arr.push('')
      } else {
        for (let i = 0; i < n; i++) {
          //  i         标识左扣号
          //  n - i - 1 标识右扣号
          generate(i).forEach(a => {
            generate(n - i - 1).forEach(b => {
              arr.push(`(${a})${b}`)
            })
          })
        }
      }
      cache[n] = arr
      return arr
    }
  
    return generate(n)
  };