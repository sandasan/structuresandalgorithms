var changeCoin = function(coin, amount) {
  //定义dp
  let dp = new Array(amount + 1 ).fill(Infinity);
  dp[0] = 0;
  for (let i = 0; i <= amount; i++) {
    for (let j = 0; j < coin.length; j++) {
        if (i >= coin[j]) {
            dp[i] = Math.min(dp[i], dp[i -coin[j]] + 1)
        }
    }
  }
  return dp[amount] === Infinity  ?  -1 : dp[amount];
}

console.log(changeCoin([1, 2, 5], 11))


