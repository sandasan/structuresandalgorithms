const str = '{[]}';


var isValidString = function(s) {
   const stack = [],
   map = {
    '(': ')',
    '[': ']',
    '{': '}',
  };

   // 循环遍历字符串
   for (const x of s) {
     if (x in map) {
        stack.push(x);
        continue;
     };
     if (map[stack.pop()]  !== x) {
        return false;
     }
   }
   
   return !stack.length
}
console.log('-->', isValidString(str));