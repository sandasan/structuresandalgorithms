树  二叉树  二叉搜索树

Tree：
               Tree
                |
                |
                A(root) --------------------- level 0

          B                C  --------------- level 1   
   (Parent Node)
    D          E        F   siblings  G ---------- level 2

H       I    J                --------------- level 3
  (child Node)
  Sub-Tree
1. Root
2. 左子树和右子树
3. 兄弟节点
4. 分层不同层

Binary Tree 
每一个节点只有2个孩子
完全二叉树
                 o
           o            o
        o    o        o    o
      o  o  o  o    o  o  o  o


图 -> 树可以往回走

二叉搜索树 
(Binary search Tree/ ordered binary tree/ sorted binary tree)
可以是空树 或者：
1. 左子树上所有的节点值小于它的根节点的值 （不是左儿子）
2. 右子树上所有的节点值大于它的根节点的值
3. 左子树和右子树也分别是二叉查找树

                      27

              14                35
 
          10     19        31         42   


 为什么需要？
 分半搜索可以只找左子树或者右子树，时间复杂度是 log2N，
 搜索更加有效率。元素排列更加有序。

                     



























