哈希表
哈希函数
哈希碰撞 (链表)
list 可以是数组和链表 里面可以有重复元素
list = [1,2,4,5,5,6]
map
map = {
    'xiaoLi' : 60
    'liming': 50
}
set
set 不允许有重复元素
set = {'jack', 'selina'}


HashMap  VS  TreeMap
HashSet  VS  TreeSet


HashMap 和 HashSet  使用的是HashTable 哈希表来存储的 查询是O(1)时间复杂度

TreeMap 和 TreeSet  使用的是binary-search-tree 二叉树来存的 查询是logN时间复杂度

使用场景：
TreeMap 和 TreeSet  的所有元素是排好序的时间较慢
HashMap 和 HashSet  是乱序的但是时间较快

HashTable  VS  binary-search-tree