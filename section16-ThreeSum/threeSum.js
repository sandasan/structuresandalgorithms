var arr = [-1, 0, 0, 1,2, -1, -1, -4, -4]
var threeSum = function(nums) {
    const len = nums.length;
    if (len < 3) return []
    // 先升序排序
    nums.sort((a, b) => a - b);
    // 定义一个set存结果
    let res = []
    for(let i = 0; i < len - 2 ; i ++ ) {
        // 若元素大于0直接跳过不用检测
        if (nums[i] > 0 ) break;
         // i 去重 若相邻的元素相等则跳过循环
        if( i > 0 && nums[i] === nums[i - 1])
        continue; 
        // 设置左边界l 和 右边界r
        let l = i + 1, r = len - 1;
        // 只要 左边界小于右边界的情况下不断执行循环
        while (l < r) {
            const sum = nums[i] + nums[l] + nums[r];
            if (sum < 0) {
                l++;
                continue;
            }
            if (sum > 0) {
                r--;
                continue;
            }
            res.push([nums[i], nums[l], nums[r]])
            // l去重 若相邻的2个元素相等的话直接右移动一位
            while(l < r  && nums[l] === nums[l+1])
            l +=1 
            // r去重 若相邻的2个元素相等的话直接左移动一位
            while(l < r  && nums[r] === nums[r-1])
            r -=1;

            l++;
            r--;
        }
    }
    // Array.from(resSet) [ '-1,-1,2', '-1,0,1' ]

    return res
}

console.log(threeSum(arr));