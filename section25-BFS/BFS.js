function Node(key) {
    this.children = [];
    this.key = key
}

/*
                  1

             2     7       8
          3    6         9    12
       4   5          10  11

*/
let n1 = new Node(1),
n2 = new Node(2)
n3 = new Node(3),
n4 = new Node(4),
n5 = new Node(5),
n6 = new Node(6),
n7 = new Node(7),
n8 = new Node(8),
n9 = new Node(9),
n10 = new Node(10),
n11 = new Node(11),
n12 = new Node(12);

n1.children.push(n2, n7, n8)
/*
 头             尾   <------
---------------------
 2  7  8
---------------------
 */
/*
                  1

             2     7       8
          3    6         9    12
       4   5          10  11

*/


//n2.children.push(n3, n6)

/*
 头             尾   <------
---------------------
 [ 1  2  7  8   3   6   9  12   4  5  10  11 ]
---------------------
 */
//n8.children.push(n9, n12)
//n3.children.push(n4, n5)
//n9.children.push(n10, n11)
  
//JavaScript实现队列,可以通过数组的方法来模拟, 进入队列使用push(), 出列使用shift()
const breadth = function (node) {
    //创建一个队列放开始节点
    const queue = [node]
    const nodeList = []
    // 只有队列不为空
    while(queue.length) {
      console.log(queue)
      //出列（每一次的头部元素出队列)从队列头部取出一个头部的元素
      const first = queue.shift();
      //console.log('first', queue, first, first.key)
      nodeList.push(first.key)
      // 入队每一次的出队列的所有孩子进入队列
      // 子节点依次从队列尾部加入，继续进行循环
      first.children.forEach(child => queue.push(child));
    }
    return nodeList
}


const result = breadth(n1);

