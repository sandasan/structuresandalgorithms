// 补差
// https://juejin.cn/post/6844903908322574344

class NodeList {
    constructor(val) {
        this.val  = val;
        this.next = null;
    }
    append(val) {
        let current = this;
        let node = new NodeList(val);
        while(current.next) {
            current = current.next
        }
        current.next = node;
    }
    print() { 
        let current = this;
        let res = [];
        while(current) {
            res.push(current.val);
            current = current.next;
        }
        console.log(res)
    }
}
// headA 4,1,8,4,5
let headA = new NodeList(4);
headA.append(1);
headA.append(8);
headA.append(4);
headA.append(5);

//console.log(headA);
//[5,6,1,8,4,5], 
let headB = new NodeList(5);
headB.append(6);
headB.append(1);
headB.append(8);
headB.append(4);
headB.append(5);

//console.log(headB);

const GetInsersection = function (headA, headB) {
    let [curA, curB] = [headA, headB];
    while(curA !== curB) {
        curA = (curA === null) ?  headB : curA.next;
        curB = (curB === null) ?  headA : curB.next;
    }
    return curA;
}


console.log(GetInsersection(headA, headB))
