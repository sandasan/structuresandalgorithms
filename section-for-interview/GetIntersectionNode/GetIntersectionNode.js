class NodeList {
    constructor(val) {
        this.val  = val;
        this.next = null;
    }
    append(val) {
        let current = this;
        let node = new NodeList(val);
        while(current.next) {
            current = current.next
        }
        current.next = node;
    }
    print() { 
        let current = this;
        let res = [];
        while(current) {
            res.push(current.val);
            current = current.next;
        }
        console.log(res)
    }
}
// headA 4,1,8,4,5
let headA = new NodeList(4);
headA.append(1);
headA.append(8);
headA.append(4);
headA.append(5);

//console.log(headA);
//[5,6,1,8,4,5], 
let headB = new NodeList(5);
headB.append(6);
headB.append(1);
headB.append(8);
headB.append(4);
headB.append(5);

//console.log(headB);

/*
相交的时候,表示在同一个地方相遇,这个地点是相同的
表明两链表中各存在一个节点指向同一段地址
那么如果我们先后把两条的链表存在在set里面
当我们在添加第二条链时
如果出现添加的节点已经存在
那就说明了两链表相交
set有用会面对一个空间消耗的问题,于是我们思考另一种办法
*/
const getIntersectionNode = function (headA, headB) {
   let set = new Set();

   while(headA) {
     set.add(headA);
     headA = headA.next;
   }

   while(headB) {
    if(set.has(headB)) return headB;
    headB = headB.next;
  }

  return null;
}

console.log(getIntersectionNode(headA, headB))




























