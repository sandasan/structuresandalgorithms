给你两个单链表的头节点 headA 和 headB ，请你找出并返回两个单链表相交的起始节点。如果两个链表不存在相交节点，返回 null 。


题目数据 保证 整个链式结构中不存在环。
![img.png](img.png)
注意，函数返回结果后，链表必须 保持其原始结构 。
eg:
![img_1.png](img_1.png)
输入：intersectVal = 8, listA = [4,1,8,4,5], listB = [5,6,1,8,4,5], skipA = 2, skipB = 3
输出：Intersected at '8'
解释：相交节点的值为 8 （注意，如果两个链表相交则不能为 0）。
从各自的表头开始算起，链表 A 为 [4,1,8,4,5]，链表 B 为 [5,6,1,8,4,5]。
在 A 中，相交节点前有 2 个节点；在 B 中，相交节点前有 3 个节点。
— 请注意相交节点的值不为 1，因为在链表 A 和链表 B 之中值为 1 的节点 (A 中第二个节点和 B 中第三个节点) 是不同的节点。换句话说，它们在内存中指向两个不同的位置，而链表 A 和链表 B 中值为 8 的节点 (A 中第三个节点，B 中第四个节点) 在内存中指向相同的位置。

eg:
![img_2.png](img_2.png)
输入：intersectVal = 2, listA = [1,9,1,2,4], listB = [3,2,4], skipA = 3, skipB = 1
输出：Intersected at '2'
解释：相交节点的值为 2 （注意，如果两个链表相交则不能为 0）。
从各自的表头开始算起，链表 A 为 [1,9,1,2,4]，链表 B 为 [3,2,4]。
在 A 中，相交节点前有 3 个节点；在 B 中，相交节点前有 1 个节点。


来源：力扣（LeetCode）
链接：https://leetcode.cn/problems/intersection-of-two-linked-lists
著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。

进阶：你能否设计一个时间复杂度 O(m + n) 、仅用 O(1) 内存的解决方案？


https://juejin.cn/post/6844903908322574344

补差：
![img_3.png](img_3.png)****
我们在headA后面添加一条headB链表
在headB后面添加一个条headA链表
即A + B = B + A
那如图所示,我们就开始同时遍历,每次遍历进行比较,当两指针相同时,即在交点相遇

let curA = headA
while (1) {
  curA = curA === null ? headB : curA.next
}
复制代码上面这段代码就表示

先遍历headA,curA = headA
遍历 curA = curA.next
遍历到headA终点了,即curA === null
就开始遍历headB,即curA = headB
遍历 curA = curA.next

那我们让curA和curB同时遍历,则他们会在第二次经历相交点时相遇
等相交时就退出便利直接返回相交点。

var getInsersectionNode = function (headA, headB) {
  let [curA, curB ] = [headA, headB];
  while(curA !== curB) {
    curA = (curA === null) ? headB : curA.next;
    curB = (curB === null) ? headA : curB.next;
  }
  return curA;
}
// 时间复杂度 O(m + n) 、仅用 O(1) 












