class NodeList {
    constructor (val) {
        this.val = val;
        this.next = null;
    }
    append (val) {
        let current = this;
        let node = new NodeList(val);
        while(current.next) {
            current = current.next
        }    
        current.next = node;
    }
    print() {
        let current = this;
        let res = [];
        while(current) {
            res.push(current.val);
            current = current.next
        }
        console.log(res);
    }

}

const node1 = new NodeList(2);
node1.append(7);
//node.append(4);
//node.append(3)
//node.append(5);
//console.log(node);
// 2  7  4  3  5

const node2 = new NodeList(2);
node2.append(-1);


function mergeTwoList (list1, list2) {
   if (list1 === null) {
     return list2;
   }
   if (list2 === null) {
     return list1;
   }
   
   if (list1.val <= list2.val) {
    list1.next = mergeTwoList( list1.next, list2);
     return list1;
   } else {
    list2.next = mergeTwoList( list2.next, list1);
     return list2;
   }
}

console.log('-->',mergeTwoList(node1, node2));


































