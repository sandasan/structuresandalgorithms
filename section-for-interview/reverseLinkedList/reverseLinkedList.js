class NodeList {
    constructor(val) {
        this.val = val;
        this.next = null;
    }
    append(val) {
       let current = this;
       let node = new NodeList(val);
       while(current.next) {
        current = current.next;
       } 
       current.next = node;
    }

    print() {
        let current = this;
        let res = [];
        while(current) {
            res.push(current.val);
            current = current.next;
        }
        console.log(res);
    }
}

let node = new NodeList(1);
node.append(34);
node.append(5);
node.append(78);
console.log(node);

// reverseLinkedList

const reverseLinkedList = function(head) {
    if (!head) return null;
    let pre = null;
    let curr = head;
    while(curr !== null) {
      let nextTemp = curr.next;
      curr.next = pre;
      pre = curr;
      curr = nextTemp;
    }

    return pre;
}

console.log(reverseLinkedList(node))