给你单链表的头节点 head ，请你反转链表，并返回反转后的链表。
![img_1.png](img_1.png)
输入：head = [1,2,3,4,5]
输出：[5,4,3,2,1]
在遍历列表时，将当前节点的 next 指针改为指向前一个元素。由于节点没有引用其上一个节点，因此必须事先存储其前一个元素。在更改引用之前，还需要另一个指针来存储下一个节点。不要忘记在最后返回新的头引用！
public ListNode reverseList(ListNode head) { // 1
    ListNode pre = null; // 2
    ListNode curr = head;  //3 
    while(curr !== null) {  //4
        ListNode nextTemp = curr.next; // 5
        curr.next = pre; // 6
        pre = curr;  // 7 
        curr = nextTemp; // 8
    }
    return pre; // 9
}

public ListNode reverseList(ListNode head) {  //1

 head
  |
  1   2  3  null


ListNode prev = null;   // 2

prev
 |
null 

将链表head赋值给curr，即curr指向head链表，可得图如下：
ListNode curr = head;

 curr
 head
  |
  1   2  3  null


把curr.next 赋值给nextTemp变量，即nextTemp指向curr的下一节点（即节点2），可得图如下：
ListNode nextTemp = curr.next; //5

 curr
  |
  1   2  3  null
      |
    nextTemp

把prev赋值给curr.next,因为prev初始化化指向null，即curr(节点1)指向了null，链表图解成这样了：

curr.next = prev;  // 6 

 curr
  |
  1 ----null        2  3  null
                    |
                  nextTemp


把curr赋值给prev，prev指向curr，
 prev = curr;  //7


curr
  |
  1 ----null
  |                 2  3  null
prev                |
                  nextTemp


把nextTemp赋值给curr，即curr指向nextTemp，图解如下：
curr = nextTemp; //8

                   curr
  1 ----null        |
  |                 2  3  null
prev                |
                  nextTemp


curr依旧不为null，我们继续图解完它。

ListNode nextTemp = curr.next; //5



                   curr
  1 ----null        |
  |                 2  3  null
prev                   |
                    nextTemp

执行完curr.next = prev;后：



curr
                   
 2   -----> 1 ----null        
            |                       3  null
            prev                    |
                                nextTemp

执行完prev = curr;后：


curr
                   
 2   -----> 1 ----null        
                               3  null
pre                            |
                             nextTemp

执行完curr = nextTemp;后：

                              curr
 2   -----> 1 ----null        
                               3  null
pre                            |
                             nextTemp

 来到这里，发现curr还是不为null，再回到while循环，再执行一遍：

                             curr                          curr
 2   -----> 1 ----null        
                               3  null
pre                                |
                                nextTemp



                            
  3 -->      2   -----> 1 ----null        
                                            null
 curr       pre                                |
                                            nextTemp
 pre
                                                                        
  3 ------->   2   -----> 1 ---> null        
                                            null
 curr                                       |
                                            nextTemp

                                                                                        nextTemp
 pre                                        curr    
                                                                        
  3 ------->   2   -----> 1 ---> null        
                                             null
                                              |
                                            nextTemp
 来到这里，我们发现curr已经为null了，可以跳出循环了。这时候prev指向的就是链表的反转呀，所以第9行执行完，反转链表功能实现：

