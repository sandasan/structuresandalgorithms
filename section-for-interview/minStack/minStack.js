
var MinStack = function() {
  this.minArr = new Array();
  this.stackArr = new Array();
};

/** 
 * @param {number} val
 * @return {void}
 */
MinStack.prototype.push = function(val) {
  this.stackArr.push(val); 
  this.minArr.push(Math.min(...this.stackArr));
};

/**
 * @return {void}
 */
MinStack.prototype.pop = function() {
    this.stackArr.pop(); 
    this.minArr.pop(); 
};

/**
 * @return {number}
 */
MinStack.prototype.top = function() {
  return this.stackArr[this.stackArr.length - 1];
};

/**
 * @return {number}
 */
MinStack.prototype.getMin = function() {
    return this.minArr[this.minArr.length - 1];
};

/**
 * Your MinStack object will be instantiated and called as such:
 * var obj = new MinStack()
 * obj.push(val)
 * obj.pop()
 * var param_3 = obj.top()
 * var param_4 = obj.getMin()
 */
































