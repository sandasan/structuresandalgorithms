class LRUCache {
   constructor(capacity) {
     //容量 
     this.capacity = capacity;
     this.cache = new Map();
   }
   get(key) {
    let hasKey = this.cache.has(key);
    if (!hasKey) {
        return -1;
    } else {
       // 拿到val
       let val = this.cache.get(key);
       // 先删除再更新
       this.cache.delete(key);
       this.cache.set(key, val);
       return val;
    }
   }
   put(key, val) {
        // put之前先检测是否缓存大于了容量 this.cache.size 
        if(this.cache.size > this.capacity) {
         this.cache.delete(this.cache.keys().next().value);
        }
        // 检测是否存在key
        let hasKey = this.cache.has(key);
        // 若存在则直接删除key
        if (hasKey) {
            this.cache.delete(key);
        }
        // 然后再重新设置
        this.cache.set(key, val);
   }
}
























