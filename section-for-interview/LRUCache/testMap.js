// https://es6.ruanyifeng.com/#docs/set-map
let map = new Map([
    [1, 'one'],
    [2, 'two'],
    [3, 'three'],
  ]);
/*
 keys方法、values方法、entries方法返回的都是遍历器对象（详见《Iterator 对象》一章）。
 由于 Set 结构没有键名，只有键值（或者说键名和键值是同一个值），所以keys方法和values方法的行为完全一致。
*/
//console.log(map.keys())  
//console.log([...map.keys()])  

//console.log(map.values())  
//console.log([...map.values()]) 

// 获取第一个（这种方法只能获取第一个）
// map.keys().next().value
//console.log(map.keys().next().value)  
//获取任意一个没有直接方法，只能通过转换思路获取（这种方法可以获取任意一个）
// Array.from(map.keys())
console.log(Array.from(map.keys()))