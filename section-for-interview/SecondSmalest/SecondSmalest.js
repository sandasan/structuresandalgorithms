function findSecondSmallest(arr) {
  var smallest = Number.MAX_VALUE;
  var secondSmallest = Number.MAX_VALUE;
  if(arr.length == 0 || arr.length < 2) 
  return secondSmallest;

  for (let i = 0; i < arr.length; i++) {
    if (smallest > arr[i]) {
        secondSmallest = smallest;
        smallest = arr[i];
    }
    
    if(smallest < arr[i] && secondSmallest > arr[i]) {
        secondSmallest =  arr[i];
    }
  }
  return secondSmallest;
}

const arr  =[5, 9];
console.log(findSecondSmallest(arr));