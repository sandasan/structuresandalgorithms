
const nums = [-1,0,3,5,9,12]; target = 3;
console.log(nums)
/*
 Math.floor((left + right) / 2); 为什么不好？会溢出 
，当我们的数超过他所规定的最值时，Integer将无法表示。
mid可能会超出该数据类型的最大值

因此在我们上述算法中，
https://www.runoob.com/js/js-obj-number.html
https://cloud.tencent.com/developer/article/1893949

left <= MAX_VALUE和 right <= MAX_VALUE是肯定的
但是left+right <= MAX_INT 我们无法确定，所以会造成栈溢出。
left <= right
right - left > 0  并且  left + (right - left)  = right
left + (right - left) / 2  <= right  

更好的方式采用 位移
mid=(left+right)>>1相当 于mid=(left+right)/2



*/
function binarySearch(nums, target) {
    let left = 0;
    let right = nums.length - 1;
   
    while(left <= right) {
        // X  Math.floor((left + right) / 2);
        // 找中间的 mid = l + (r - l) / 2 
        // let mid = Math.floor((left + right) / 2);
        // let mid = Math.floor((right - left ) / 2 ) + left
        let mid = (left + right) >> 1;
        // 比较若目标数大于中间的数则在右半边 则 left = mid + 1
        // 比较若目标数小于中间的数则在左半边 则 right = mid - 1
        if (target > nums[mid]) {
            left = mid + 1;
        } else if (target < nums[mid]) {
            right = mid - 1
        } else {
            return mid;
        }
    }
    return -1;
}

console.log(binarySearch(nums, target));















