给定一个 n 个元素有序的（升序）整型数组 nums 和一个目标值 target  ，写一个函数搜索 nums 中的 target，如果目标值存在返回下标，否则返回 -1。
输入: nums = [-1,0,3,5,9,12], target = 9
输出: 4
解释: 9 出现在 nums 中并且下标为 4

输入: nums = [-1,0,3,5,9,12], target = 2
输出: -1
解释: 2 不存在 nums 中因此返回 -1

使用二分查找

 有序 单调递增或者递减
 有明确上下阶
 可以通过索引访问 数组

  left = 0 right = len(array) -1
  // 若左界小于右界
   while(left <= right) {
    // 得到中间数的下标
    mid = (left + right) / 2

    if (array[mid] === target) {
        return result;
    }
    // 若没有找到和中间的数进行比较 
    // 目标数大于中间数 去数组右半部分查找
    // 目标数小于中间数 去数组左半部分查找
    if (targe > array[mid]) {
        left =  mid  + 1;
    }  else {
        right = mid - 1;
    }
  }









来源：力扣（LeetCode）
链接：https://leetcode.cn/problems/binary-search
著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
