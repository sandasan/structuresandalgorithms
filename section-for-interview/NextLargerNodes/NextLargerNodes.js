class NodeList {
    constructor (val) {
        this.val = val;
        this.next = null;
    }
    append (val) {
        let current = this;
        let node = new NodeList(val);
        while(current.next) {
            current = current.next
        }    
        current.next = node;
    }
    print() {
        let current = this;
        let res = [];
        while(current) {
            res.push(current.val);
            current = current.next
        }
        console.log(res);
    }

}

const node = new NodeList(2);
node.append(7);
//node.append(4);
//node.append(3)
//node.append(5);
//console.log(node);
// 2  7  4  3  5


const nextLargerNodes = function (head) {
    let valArr = [];
    while(head) {
        valArr.push(head.val);
        head = head.next;
    }
    console.log(valArr);
    let index = 0;
    let res = [];
    while(valArr.length) {
        // 取头部元素
        const headVal = valArr.shift();
        //console.log(headVal);
        const bigVal =  valArr.find((item) => item > headVal);
        console.log(bigVal);
        res[index] = bigVal > 0 ? bigVal : 0;
        index++;
    }

   return res;

}

console.log(nextLargerNodes(node));




















