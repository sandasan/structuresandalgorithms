const arrayTest  = [ 85, 24, 63, 45, 17, 31, 96, 50 ];

function quickSortRecursive(arr, L, R) {
  if (L >= R) {
    return [];
  }
  let left = L; right = R;
  // 以最左边作为基准值
  let pivot = arr[left];
  //  [7, -2, 2, 56]
  // 若左界小于右界
  while (left < right) {
    // 若左界小于右界并且右边的值大于基准值，右指针向左移动
     while (left < right && arr[right] > pivot) {
        right--;
     }   
     // 将大于中心轴的数字放在 pivot右边

     if (left < right) {
        arr[left] = arr[right];
     }
     
     while(left < right && arr[left] <= pivot) {
        left++;
     }

     // 将小于中心轴的数字放在 pivot左边
     if (left < right) {
        arr[right] = arr[left];
     }

     if (left >= right) {
        arr[left] = pivot;
     }
  }
  quickSortRecursive(arr, L, right - 1);
  quickSortRecursive(arr, right + 1, R);
}

array = [7, -2, 2, 56]
quickSortRecursive(array, 0, array.length - 1)

console.log(array)












