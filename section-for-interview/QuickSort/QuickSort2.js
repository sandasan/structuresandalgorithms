// ref: https://www.bilibili.com/video/BV1WF41187Bp/?spm_id_from=333.788&vd_source=b2f17b07a5bf5a21fe0bf57c643d76b9
const arrayTest  = [ 85, 24, 63, 45, 17, 31, 96, 50 ];

function quickSortRecursive(arr, low, high) {
    if (low < high) {
         // 取中间的正确位置
         let mid = partition(arr, low, high);
         // 左半
         quickSortRecursive(arr, low, mid -1);
         // 右半
         quickSortRecursive(arr, mid+1, high);
    }
}
  // 把比pivot小的换到前面
function partition(arr, start, end) {

  // 取最后一个元素作为pivot
  const pivotValue = arr[end];
  let pivotIndex  = start;
  
  /*
  //  i
  //  2             4   5   1   7  6  3（pivot）
  // pivotIndex
   发现 2 < 3 则换位置
  */
  // 有2个指针 i 和  pivotIndex
  for (let i = start; i < end; i++) {
     // 把比pivot小的换到前面
     if (pivotValue > arr[i]) {
      // 实质上是2个指针 i, pivotIndex都是往后面扫描, i 扫描到比pivot小的就 调换i和j的位置
       [arr[i], arr[pivotIndex]] = [arr[pivotIndex], arr[i]];
       pivotIndex++;
     }   
  }
  // 交换基准值，让基准值在正确的排序位置
  [arr[pivotIndex], arr[end]] = [arr[end], arr[pivotIndex]];
  return pivotIndex;
}

array = [7, -2, 2, 56]
quickSortRecursive(array, 0, array.length - 1)

console.log(array)












