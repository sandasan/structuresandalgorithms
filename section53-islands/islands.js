var numIslands = function(grid) {
    var count = 0;
    for (var i = 0; i < grid.length; i++) {
      for (var j= 0; j < grid[i].length; j++){
        if (grid[i][j] === '1') {
            count++;
            dfsSearch(grid, i, j);
        }
      }
    }
    return count;
}

function dfsSearch(grid, i, j) {
    //如果是0或者已经被遍历过，不继续遍历，直接返回
    if (grid[i][j] === '-1' || grid[i][j] === '0') {
        return;
    }
     //将遍历到的元素置为-1,表示遍历过
     grid[i][j] = '-1';

     //对4个上下左右进行遍历
     // 右边的方向
     if (i + 1 < grid.length) {
        dfsSearch(grid, i + 1, j)
     }
      // 上边的方向
     if (j+1 < grid[0].length) {
        dfsSearch(grid, i, j + 1)
     }
       // 左边的方向
     if(i - 1>=0) {
        dfsSearch(grid, i -1 , j)
     }
       // 下边的方向
     if(j - 1>=0) {
        dfsSearch(grid, i , j-1)
     }
}

var grid = [
    ["1","1","1","1","0"],
    ["1","1","0","1","0"],
    ["1","1","0","0","0"],
    ["0","0","0","0","0"]
  ]
var grid1 = [
    ["1","1","0","0","0"],
    ["1","1","0","0","0"],
    ["0","0","1","0","0"],
    ["0","0","0","1","1"]
  ]

 console.log(numIslands(grid1));
