// 构建小顶堆
 //  9  2  13 
/*
    2

  9    13  
*/

function buildHeap(arr, len) {
 
  let lastNodeIndex = len - 1;
  console.log('lastNodeIndex', lastNodeIndex)
  let lastNodeParentIndex =  (lastNodeIndex - 1);
  for (let i = lastNodeParentIndex; i >= 0 ; i--){
    heapify(arr, len, i);
  }
} 
// 交换位置
function sawp(arr, i , j) {
  [arr[i], arr[j]] = [arr[j], arr[i]]
}

// 调整堆
/*
   MinHeap
              51
        
        12            8
 leftChildIndex  rightChildIndex
*/
function heapify(arr, len, i) {
   while(true) {
     // [51, 12, 8]
    if (i >= len) return;
    let leftChildIndex = 2 * i + 1;
    let rightChildIndex = 2 * i + 2;

    //                   （max）
    //  leftChildIndex     i     rightChildIndex
    let max = i;
    
    if (leftChildIndex < len && arr[leftChildIndex] < arr[max]) {
      max = leftChildIndex;
    }
    if (rightChildIndex < len && arr[rightChildIndex] < arr[max]) {
      max = rightChildIndex;
    }

    if (max != i) {
      sawp(arr, max, i)
    } else {
      return;
    }
   }
}

var KthLargest  = function (k, nums) {
  // 降序排列 4, 51, 8, 12
  this.nums = nums.sort((a, b) => b - a)
  this.k = k;
 
  // [4, 51, 8, 12]
  if (this.nums.length > this.k) {
    // 要保证heap的元素个数始终等于K
    this.nums.length  = this.k
    // 构建堆 保证堆的最大长度是3
    // [ 51, 12, 8]
    console.log(this.nums, this.nums.length);
    buildHeap(this.nums, this.nums.length)
  }
}

KthLargest.prototype.add = function (val) {
  console.log(this.nums.length < this.k)
  // [ 8, 12, 51 ]
  if (this.nums.length < this.k) {
    this.nums.push(val);
    buildHeap(this.nums, this.nums.length)
    if (this.nums.length < this.k) { 
      return;
    } else {
      return this.nums[0]
    }
  }
  // [ 8, 12, 51 ]
  // val --> 43
  if (val <= this.nums[0]) {
    console.log(this.nums, this.nums )
    return this.nums[0]
  }
  this.nums[0] = val;
  heapify(this.nums, this.nums.length, 0)

  return this.nums[0];

}

var k = 3
var arr = [4, 51, 8, 12]
var kthLargest = new KthLargest(k, arr)
console.log(kthLargest.nums , kthLargest.nums[0])
//console.log(kthLargest.add(43))





