https://juejin.cn/post/7028592013477937183
返回流中的第k大的元素
eg:
{
    k= 3
    [4, 5, 8, 2]
}
返回流中[4, 5, 8]的第3大的元素 是 4

1. 保存K的最大每一次进行排序
 N * K * logk
2. 优先队列  （要保证minheap的元素的个数始终等于K）

 当k=3 首先进入优先队列是 4， 5， 8 ，4在栈顶
 要保证heap的元素个数始终等于K
        MinHeap
           4
        
        5      8
当2进来的时候 发现2比栈顶的元素4小，则舍弃
只需要一次性操作把元素踢掉 O(1)
           4
        
        5      8
当10进入的时候发现比栈顶元素4要大，则剔除栈顶元素4
，把10加进来进行重新排序
         10

      5      8    

然后重新排序
          5

      10      8 
调整的复杂度是 N * log2K


输入：
["KthLargest", "add", "add", "add", "add", "add"]
[[3, [4, 5, 8, 2]], [3], [5], [10], [9], [4]]
输出：
[null, 4, 5, 5, 8, 8]
解释：
KthLargest kthLargest = new KthLargest(3, [4, 5, 8, 2]);
kthLargest.add(3);   // return 4
kthLargest.add(5);   // return 5
kthLargest.add(10);  // return 5
kthLargest.add(9);   // return 8
kthLargest.add(4);   // return 8

















