const s = [22]

var insertSort = function (arr) {
   // 遍历数组从下标是1的位置开始
   for (let i = 1; i < arr.length; i ++ ) {
      // 下标是 i 的数据为待插入的元素
      // 要找到待插入的位置
      // 从8开始
      let crurrentNumber = arr[i];
      // 待插入的位置 默认当前为插入位置是i，index一直左移每一次减1，直到找到位置
      let insertIndex = i - 1;
      // 第一次 arr[1] = 8 insertIndex = 0
      // 找合适的位置 插入的数据比移动下标的数据大
      // insertIndex = 0 >= 0 arr[0] == 22 > 8
      while(insertIndex >= 0 && arr[insertIndex] > crurrentNumber) {
        // insertIndex = 0 >= 0 arr[0] == 22 > 8
        // arr[1] = arr[0] 交换位置 把8和22位置替换
        // 此时arr[0] = 8  arr[1] = 22
        arr[insertIndex + 1] = arr[insertIndex];
        // 此时 insertIndex = -1
        insertIndex--
      }
      // 此时 insertIndex = -1 arr[-1] = 8
      arr[insertIndex + 1] = crurrentNumber
   }
   return arr;
} 

console.log(insertSort(s));
