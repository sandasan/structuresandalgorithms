// 二叉树层次遍历
给你二叉树的根节点 root ，返回其节点值的 层序遍历 。 （即逐层地，从左到右访问所有节点）。
输入：root = [3,9,20,null,null,15,7]
输出：[[3],[9,20],[15,7]]
输入：root = [1]
输出：[[1]]

输入：root = []
输出：[]

              3
          9      20
              15       7
              
输入：root = [3,9,20,null,null,15,7]
输出：[[3],[9,20],[15,7]]

方案01:
BFS 广度优先搜索
怎么判定这个节点是这一层的末节点？已经结束了
1. 把每一层的信息加到level上去 queue 不需要在queue里面加
2. 在进行BFS的时候加一层for进行一层一层扫
     batch process

BFS每一个节点会访问一次并且仅此一次。

方案02:
DFS 深度优先搜索
DFS循环体里面本身要把level放进去
先走3 记住level 然后再走9 记住level，然后回到3
走到20 记住level 然后走15 记住level  返回到 20 ，
最后走到 7 记住level
[
   []
   []
   []
]

[
   [3]
   []
   []
]

[
   [3]
   [9, 20]
   [15, 7]
]



























