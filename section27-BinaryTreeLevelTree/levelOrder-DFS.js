function Node(val, left, right) {
    this.val = (val===undefined ? 0 : val)
    this.left = (left===undefined ? null : left)
    this.right = (right===undefined ? null : right)
}

const n1 = new Node(3),
n9 = new Node(9),
n10 = new Node(10),
n11 = new Node(11),
n20 = new Node(20),
n15 = new Node(15),
n7 = new Node(7)
n25 = new Node(25)
n6 = new Node(6)

n1.left = n9
n1.right = n20

n9.left = n25
n9.right = n6

//n20.left = n15
//n20.right = n7


//console.log(n1)
/*
        3
    9        20
10  

        Node {
            val: 3,
            left: Node { val: 9, left: null, right: null },
            right: Node {
                val: 20,
                left: Node { val: 15, left: null, right: null },
                right: Node { val: 7, left: null, right: null }
            } 
        }
*/

/* 深度优先遍历 */

// array = []
// [ [], [], [] ] 变成2维数组 中间的放不同的层级
/*
         3
    9        20
10
 /*
    第一次
    level: 0
    array: [[]]
    root: {
        val: 3,
        left: Node {
            val: 9,
            left: Node { val: 10, left: null, right: null },
            right: null
        },
        right: Node { val: 20, left: null, right: null }
     }
     array: [[3]]
    第二次 
    level: 0 + 1 = 1
    array: [[3]]
    root: left: Node {
        val: 9,
        left: Node { val: 10, left: null, right: null },
        right: null
    }
    array: [[3], []] -> array: [[3], [9]]
    第三次 
     root: { val: 10, left: null, right: null },
     level: 0 + 1 + 1 = 2
     array: [[3], [9]]
     array: [[3], [9], []]
     array: [[3], [9], [10]]
     第四次 
     root: { val: 10, left: null, right: null },
     level: 0 + 1 + 1 + 1 = 3
     array: [[3], [9]]
     array: [[3], [9], []]
     array: [[3], [9], [10]]
    */ 
var helpFun = (root, level, array)  => {
    if (root === null) {
        return []
    } else {
       //全局数组中，如果没有初始化改成，创建一层
        if (!array[level]) {
            array[level] = []
        }
        array[level].push(root.val)
        //层级+1，递归左边子树 //层级+1，递归左边子树
        console.log('-->', array)
        
        helpFun(root.left,  level + 1, array)
        helpFun(root.right,  level + 1, array) 
    }
}
var levelOrder =  function(root) {
    if (root === null) return [];
    var array = [];
    //遍历第一层
    helpFun(root, 0, array)
    console.log(array)
    return array;
}

levelOrder(n1)

/*
   // 因为每一次都是先遍历左边的
    --> [ [ 3 ] ]
    --> [ [ 3 ], [ 9 ] ]
    --> [ [ 3 ], [ 9 ], [ 25 ] ]
    --> [ [ 3 ], [ 9 ], [ 25, 6 ] ]
    --> [ [ 3 ], [ 9, 20 ], [ 25, 6 ] ]
    [ [ 3 ], [ 9, 20 ], [ 25, 6 ] ]
*/