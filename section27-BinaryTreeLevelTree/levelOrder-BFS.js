function Node(val, left, right) {
    this.val = (val===undefined ? 0 : val)
    this.left = (left===undefined ? null : left)
    this.right = (right===undefined ? null : right)
}

const n3 = new Node(3),
n9 = new Node(9),
n10 = new Node(10),
n11 = new Node(11),
n20 = new Node(20),
n15 = new Node(15),
n7 = new Node(7)
n6 = new Node(6)
n25 = new Node(25)


n3.left = n9
n3.right = n20

//n9.left = n25
//n9.right = n6
//n20.left = n15
//n20.right = n7


//console.log(n1)
/*
        3
    9        20
10  
*/

/* 广度优先遍历 */
var levelOrder = function(root) {
    //全局数组
    const result = [];
    if (!root) {
        return result;
    }
    //临时栈，先进后出
    const stack = [];
    stack.push(root);
    
    while (stack.length !== 0) {
        //获取当前层所有的节点，用来遍历，也用来标识当前层的节点数量，用来结束该层的循环。
        const currentLevelNodeSize = stack.length;
        console.log(currentLevelNodeSize)
        //初始化该层的保存节点的数组
        result.push([]);
        for (let i = 1; i <= currentLevelNodeSize; ++i) {
            //弹出一个节点
            const node = stack.shift();
            //全局数组中保存该层的所有节点
            result[result.length - 1].push(node.val);
            //栈中加入下一层的节点
            if (node.left) {
                stack.push(node.left);
            }
            if (node.right)  {
                stack.push(node.right);
            }
        }
    }
    return result;
}

/*
           


 */



console.log(levelOrder(n3))


