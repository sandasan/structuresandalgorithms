动态规划
Dynamic Programming (动态递推)
1. 递归 + 记忆化 递推
2. 状态的定义：opt[n], dp[n], fib[n]
3. 状态转移方程： 
opt[n] = best_of(opt[n-1], opt[n-2], ...)
4. 最优子结构

0, 1, 1, 2, 3, 5, 8, 13, 21
递推公式：
F(n) = F(n-1) + F(n-2)

int fib(int n ) {
   if(n <= 0) {
     return 0
   }
   if(n == 1) {
     return 1
   } else {
      return fib(n-1) + fib(n-2)
   }
}

 当n等于6                                         fib(6)
                                      /                             \                        
                                    fib(5)                            fib(4) 
                           /                    \                   /       \
                      fib(4)                  fib(3)            fib(3)      fib(2)
                    /        \                /      \          /   \         /  \
                 fib(3)       fib(2)      fib(2)   fib(1)    fib(2) fib(1) fib(1)fib(0)
                 /     \       /    \       /  \              /   \       
               fib(2) fib(1) fib(1)fib(0) fib(1)fib(0)     fib(1) fib(0)
                / \
           fib(1)  fib(0)

           节点个数
            level 1   1
            level 2   2
            level 3   4
            level 4   8
            ...
           呈现 2^n递增。

   优化

存一下上一次的值
int fib(int n, int [] memo ) {
   if(n <= 0) {
     return 0
   }
   if(n == 1) {
     return 1
   }  else if (!memo[n]) {
     memo[n] = fib(n-1) + fib(n-2)
   }
   return memo[n]
}
                                  
                                  fib(6)
                                  /    \   
                               fib(5)  fib(4)(存)
                                /  \
                            fib(4)  fib(3)(存)
                             /  \
                         fib(3) fib(2)(存)
                         /    \ 
                        fib(2) fib(1)
                        /    \
                      fib(1) fib(0)


 采用递推的方式从下往上，倒着去计算。
 倒转递推:
   F(0) = 0 F(1) = 1
   // i从2开始不是从6开始
   for(int i = 2; i <=n; ++i) {
    F[i] = F(i-1)+  F(i-2)
   }




