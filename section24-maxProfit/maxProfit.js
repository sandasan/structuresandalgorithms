
// 贪心算法 判定今天是否可以购买
var maxProfit = function(prices) {
    let max = 0 ;
    for (let i = 0 ; i < prices.length; i ++) {
        if (prices[i] > prices[i -1])  {
            // (prices[i] - prices[i -1]) 今天赚的利润进行累加
            max = max + (prices[i] - prices[i -1])
        }
    }
    return max;
} 

console.log(maxProfit([1, 2, 3, 4 ,5]))