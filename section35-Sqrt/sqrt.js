/**
 * @param {number} x
 * @return {number}
 */

var mySqrt = function(x) {
    if(x === 0 || x === 1) return x;
    // Math.ceil 向上取整 
    // Math.ceil(11.46) = Math.ceil(11.68)=Math.ceil(11.5)=12
    let l = 0, 
    //  0 1 2 3 4 5 6 7 8
    //  0         5
    r = Math.ceil( x / 2 + 1);
     //  0 1 2 3 4 5 6 7 8
     //        3   5
    while( l <= r ) {
         // 往向下取整數
         // 0 + ( 5-0 )/2  ==> 2
         const m = Math.floor(l +  (r - l ) / 2)
         if (m * m === x) {
             return m;
         } else if (m * m < x) {
             l = m + 1;
         } else {
             r = m - 1;
         }
    }
    return r;
 }
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
   
 
 
 
 