// 实现 pow(x, n) ，即计算 x 的整数 n 次幂函数（即，xn ）。

/*
输入：x = 2.00000, n = 10
输出：1024.00000

输入：x = 2.10000, n = 3
输出：9.26100

输入：x = 2.00000, n = -2
输出：0.25000
解释：2-2 = 1/22 = 1/4 = 0.25

递归
x * x * x * x * x

当n是偶数的情况下 中间是 2/n
y = x ^ (2/n)  y * y

当n是基数的情况下 中间是 2/n
x * x * x  （2/n）
   x
x * x * x  （2/n）
y = x ^ (2/n)  y * y * x
*/

var quickMul = function(x, m) {
    if (m === 0) return 1;
    let res = quickMul(x, Math.floor(m/2));
    if (m % 2 === 0) {
        return res * res;
    }  else {
        return res * res  *  x;
    }
}

var mPow = function(x, n) {
    let m = n;
    return m >=0 ? quickMul(x, m) : 1 /  quickMul(x, -m)
}

console.log(mPow(2, -4))