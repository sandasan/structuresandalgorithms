const maxProfix = function(prices) {
    let n = prices.length;
    let buy = -prices[0]; // 手中有股票
    let sell = 0; // 手中没有股票
    let profix_freeze = 0; //冷冻期的利润
    for (let i = 1; i < n; i++) {
        let temp = sell;
        sell = Math.max(sell, buy + prices[i]);
        buy = Math.max(buy, profix_freeze - prices[i]);
        profix_freeze = temp;
    }
    return sell;
}

var prices =  [1,2,3,0,2];
console.log(maxProfix(prices));












