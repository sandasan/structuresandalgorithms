股票买卖的问题
121 122 123 309 188 714
[7, 1, 5, 3, 6, 4]
每一次只能买一次卖一次
每一次只能买无数次
交易K次怎么解决?

动态规划DP

最多每天可以交易K次
最多持有一股

状态的定义：

DP[i]表示到了i天的最大的利润是什么 maxProfit
DP[i]

MP[i]要的结果就是到了第n天的最大利润
下标从0开始就是第n-1天的最大利润
MP[n-1]


方程：

MP[i-1]标识前一天的最大利润，
再买上这个股票, 若说卖了这个股票就是正的

MP[i] = MP[i-1] + (-a[i])
MP[i] = MP[i-1] + (a[i])
第i天可以有2个操作，
买上这个股票或者卖了这个股票。
没法进行状态的推导。

改进：

需要记录手上是否还有股票

MP[i]
i 要的结果就是到了第n天的最大利润 i(0. n-1)
j 标识手上是否还有股票是0或者1， 0没有股票 1标识有股票 j(0,1)

重新递推:
首先是没有股票的最大值是什么？
             (MP[i-1][0]标识前一天没有股票的最大值 )
             (MP[i-1][1]标识前一天有股票，然后加 不动 )

               MP[i-1][0] 不动
MP[i][0] =  MAX 
               MP[i-1][1] + a[i] //卖掉
            
               (MP[i-1][1]标识前一天有股票，然后加 不动 )

               MP[i-1][1] // 不动
MP[i][1] =  MAX 
               MP[i-1][0] + (-a[i]) //买
               (MP[i-1][1]标识前一天没有股票，然后再买一股)

存在的问题是什么？
根据上面的递推我们没有办法知道 我卖了之后然后再买进来我不知道我交易了多少次。
MP[i-1][0] + (-a[i]) 当买的情形下，要知道之前是不是小于K次的交易。

还要记录交易了多少次

MP[i]
i 标识到了第n天的最大利润 i(0. n-1)
j 标识手上是否持有股票 0或者1， 0没有股票 1标识有股票 j(0,1)
k 标识之前一共交易了多少次 k(0,k)
MP[i][j][k] 需要3维的状态递推上去。

最难


MP[i][k][j] = 
    for i 0 --> n-1
       for k 0--> K

MP[i, k, 0]  第i天交易K次手上没有持股票
所以需要上前一天也没有股票不动然后过来
或者是前一天有股票，然后卖掉1股。

                     MP[i-1, k, 0] 不动
    MP[i, k, 0]= max
                     MP[i-1, k-1, 1]  + a[i] 卖掉一次


需要上前一天也没有股票不动然后过来
或是前一天买入一股进来。

                      MP[i-1, k, 1] 不动
    MP[i, k, 1] = max
                      MP[i-1, k-1, 0] + （-a[i]） // 买入


最大值是到n-1天的时候遍历0到K，最后没有股票。
清空的
最大值是 MP[n-1, {0, k}, 0]

![img.png](img.png)








