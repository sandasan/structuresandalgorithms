//第i天不持有 由 第i-1天不持有然后不操作 和 第i-1天持有然后卖出 两种情况的最大值转移过来
// dp[i][k][0] = Math.max(dp[i - 1][k][0], dp[i - 1][k][1] + prices[i])
//第i天持有 由 第i-1天持有然后不操作 和 第i-1天不持有然后买入 两种情况的最大值转移过来
// dp[i][k][1] = Math.max(dp[i - 1][k][1], dp[i - 1][k - 1][0] - prices[i])
// k 对结果有影响 不能舍去，只能对 k 进行循环
/*
for (let i = 0; i < n; i++) {
    for (let k = maxK; k >= 1; k--) {
      dp[i][k][0] = Math.max(dp[i - 1][k][0], dp[i - 1][k][1] + prices[i]);
      dp[i][k][1] = Math.max(dp[i - 1][k][1], dp[i - 1][k - 1][0] - prices[i]);
    }
  }
  */

  const maxProfix = function (prices) {
    let buy_1 = -prices[0], sell_1 =0
    let buy_2 = -prices[0], sell_2 =0
    let n = prices.length;
    for (let i = 1; i < n ; i++) {
      sell_2 = Math.max(sell_2, buy_2 + prices[i])
      buy_2 = Math.max(buy_2, sell_1 - prices[i])

      sell_1 = Math.max(sell_1, buy_1 + prices[i])
      buy_1 = Math.max(buy_1,  - prices[i])
    }

    return sell_2;
  }

var prices = [3,3,5,0,0,3,1,4]
var prices1 = [1,2,3,4,5]
console.log(maxProfix(prices1));








