//https://xie.infoq.cn/article/932c81d5b9ff0051d2b2e2c1a
//时间复杂度O(n) 空间复杂度O(n)，dp数组第二维是常数
// 第i天不持有 由 第i-1天不持有然后不操作 和 第i-1天持有然后卖出 两种情况的最大值转移过来
// dp[i][1][0] = Math.max(dp[i - 1][1][0], dp[i - 1][1][1] + prices[i])


//第i天持有 由 第i-1天持有然后不操作 和 第i-1天不持有然后买入 两种情况的最大值转移过来
// dp[i][1][1] = Math.max(dp[i - 1][1][1], dp[i - 1][0][0] - prices[i])
//             = Math.max(dp[i - 1][1][1], -prices[i]) 
// k=0时 没有交易次数，dp[i - 1][0][0] = 0
// k是固定值 1，不影响结果，所以可以不用管，简化之后如下
//dp[i][0] = Math.max(dp[i - 1][0], dp[i - 1][1] + prices[i])
//dp[i][1] = Math.max(dp[i - 1][1], -prices[i])


const maxProfix = (prices) => {
   let n = prices.length;
   let dp = Array.from(new Array(n), () => new Array(2).fill(0))
   console.log(dp)
   // 第0天不持有
   dp[0][0] = 0;
   // 第0天持有
   dp[0][1] = -prices[0];
   for (let i = 1; i < n ; i++) {
     dp[i][0] = Math.max(dp[i-1][0], dp[i-1][1] + prices[i]);
     dp[i][1] = Math.max(dp[i-1][1], -prices[i]);
   }
   return dp[n-1][0]
}
const prices = [3,2,6,5,0,3]

console.log(maxProfix(prices))