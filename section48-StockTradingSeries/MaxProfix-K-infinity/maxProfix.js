//第i天不持有 由 第i-1天不持有然后不操作 和 第i-1天持有然后卖出 两种情况的最大值转移过来
// dp[i][k][0] = Math.max(dp[i - 1][k][0], dp[i - 1][k][1] + prices[i])
//第i天持有 由 第i-1天持有然后不操作 和 第i-1天不持有然后买入 两种情况的最大值转移过来
// dp[i][k][1] = Math.max(dp[i - 1][k][1], dp[i - 1][k - 1][0] - prices[i])
// k 同样不影响结果，简化之后如下
// dp[i][0] = Math.max(dp[i - 1][0], dp[i - 1][1] + prices[i])
// dp[i][1] = Math.max(dp[i - 1][1], dp[i - 1][0] - prices[i])

const maxProfix = function(prices) {
    let len = prices.length;
    let dp = Array.from(new Array(len), () => new Array(2).fill(0))
    console.log(dp)
    //第0天 不操作
    dp[0][0] = 0
    //第0天买入一股
    dp[0][1] = -prices[0];
    // i从第1天开始
    for (let i = 1; i < len ; i++) {
        dp[i][0] = Math.max(dp[i-1][0], dp[i-1][1] + prices[i])
        dp[i][1] = Math.max(dp[i-1][1], dp[i-1][0] - prices[i])
    }
    return dp[len-1][0]
}

var prices = [7,6,4,3,1]
//console.log(maxProfix(prices))

// 优化


const maxProfixOpt1 = function(prices) {
    let len = prices.length;
    let dp = Array.from(new Array(len), () => new Array(2))
    console.log(dp)
    //第0天 不操作
    dp[0] = 0
    //第0天买入一股
    dp[1] = -prices[0];
    // i从第1天开始
    for (let i = 1; i < len ; i++) {
        dp[0] = Math.max(dp[0], dp[1] + prices[i])
        dp[1] = Math.max(dp[1], dp[0] - prices[i])
    }
    return dp[0];
}
console.log(maxProfixOpt1(prices))