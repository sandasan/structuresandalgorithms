////每次交易要支付手续费 我们定义在卖出的时候扣手续费
//dp[i][0] = max(dp[i - 1][0], dp[i - 1][1] + prices[i] - fee)
//dp[i][1] = max(dp[i - 1][1], dp[i - 1][0] - prices[i])
var maxProfix = function(prices, fee) {
   let sell = 0;
   let buy = -prices[0];
   for (let i = 1; i < prices.length; i++) {
     sell = Math.max(sell, buy + prices[i] - fee);
     buy = Math.max(buy, sell - prices[i])
   }
   return sell;
}

var prices = [1, 3, 2, 8, 4, 9]
var prices1 =  [1,3,7,5,10,3]
console.log(maxProfix(prices, 2));
console.log(maxProfix(prices1, 3));