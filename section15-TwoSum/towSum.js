/*
输入：nums = [2,7,11,15], target = 9
输出：[0,1]
解释：因为 nums[0] + nums[1] == 9 ，返回 [0, 1] 。

来源：力扣（LeetCode）
链接：https://leetcode.cn/problems/two-sum
著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
*/
var  nums = [3,7,11,2], target = 9;
const sum = function(arr, target) {
    let hash = {}
    for (let i = 0 ; i < arr.length ; i ++ ){
        // i + j = target 
        // j = target - nums[i]
         // key (数值): value(下标)
        hash[nums[i]] = i;
        if (hash[target - nums[i]] !== undefined) {
            return [i, hash[target - nums[i]]]
        }
    }
    return []
}
console.log(sum(nums, target))