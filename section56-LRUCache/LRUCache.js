class LRUCache {
    // capacity以正整数作为容量capacity 初始化 LRU 缓存
    constructor(capacity) {
        this.capacity = capacity;
        // cache做缓存使用
        this.cache = new Map()
    }
    // int get(int key) 如果关键字 key 存在于缓存中，则返回关键字的值，否则返回 -1 。
    get(key) {
       //map的has方法
       const isHasKey = this.cache.has(key);
       if(!isHasKey) {
         return -1;
       } else {
          const val = this.cache.get(key)
           //此处需先删除再重新插入已更新使用顺序
          this.cache.delete(key);
          this.cache.set(key, val)
          return val;
       }
    }
    put(key, val) {
        // 若缓存的值大于了容量则进行删除   
        if(this.cache.size >= this.capacity) {
            // https://segmentfault.com/a/1190000010470987
            // https://es6.ruanyifeng.com/#docs/set-map#Map
            // 删除最久没有使用的key
            // 9.删除前n个值 aa = new Map(Array.from(aa).slice(3));
             /*
             for (let i = 0; i < 3; i ++) {
               aa.delete(aa.keys().next());
             }
             */
            this.cache.delete(this.cache.keys().next().value)
        }
        // 若存在key 则需要先删除再设置更新顺序
        const isHasKey = this.cache.has(key)
        if(isHasKey) {
            this.cache.delete(key)
        }
        this.cache.set(key, val)
    }
}













