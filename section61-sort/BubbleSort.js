
const arrayTest  = [ 85, 24, 63, 45, 17, 31, 96, 50 ];
function BubbleSort(arr) {
  let len = arr.length;
  for (let i = 0; i < len - 1; i++) {
    for (let j = 0; j < len - 1 - i; j++) {
        // 23 34 69    45  67
        //       j     j+1
        if (arr[j] > arr[j + 1]) {
            let tmp = arr[j + 1];
            arr[j+1] = arr[j];
            arr[j] = tmp;
        }
    }
  }
  return arr;
}

console.log(BubbleSort(arrayTest));