// A 数组
// p 数组起始下标
// r 数组结束下标 + 1
const arrayTest  = [ 85, 24, 63, 45, 17, 31, 96, 50 ];

function quickSort (A, p = 0, r) {
  // 数组结束下标 
  r = r || A.length;
  // 数组开始下标要小于结束下标
  if( p < r - 1) {
    // 最后我们会得到一个由i指针作为分界点，分割成从下标0-i，和 i+1到最后一个元素。
    const q = divide(A, p, r);
    quickSort(A, p, q);
    quickSort(A, q + 1, r);
  }
  return A;
}
/**
 *
 * @param {*} A  数组
 * @param {*} p  起始下标
 * @param {*} r  结束下标 + 1
 */
function divide(A, p, r) {
  // 最后一个元素
  const x = A[r-1];
  // 初始化i = -1
  let i = p - 1;

  for (let j = p; j < r - 1; j++) {
    // 循环数组，找到比支点小的数就将i向右移动一个位置，同时与下标i交换位置
    if (A[j] <= x) {
       i++;
       swap(A, i, j);
    }
  }
  
  //循环结束后，最后将支点与i+1位置的元素进行交换位置
  swap(A, i + 1, r - 1);

  return i + 1;
}

function swap(A ,i, j) {
    const t = A[i];
    A[i] = A[j];
    A[j] = t;
}

console.log('-->',quickSort(arrayTest, 0, 8))