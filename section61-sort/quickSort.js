// 实现方式01
// 以上代码的实现方式是，选择一个中间的数字为基准点，用两个数组分别去保存比基准数小的值，和比基准数大的值，最后递归左边的数组和右边的数组，用concat去做一个数组的合并。
const arrayTest  = [ 85, 24, 63, 45, 17, 31, 96, 50 ];
var quickSort = function (arr) {
    if (arr.length <= 1) {
        return arr;
    }

    // 取中间的数字 length
    // let pivotIndex = Math.floor(arr.length / 2);
    let pivotIndex = (left + right) >> 1;
    // arr.splice(pivotIndex, 1)返回的是删除的数组需要拿到这个值
    let pivot = arr.splice(pivotIndex, 1)[0];

    let left = [];
    let right = [];

    for (let i = 0; i< arr.length; i++) {
        if (arr[i] < pivot) {
            left.push(arr[i]) 
        } else {
            right.push(arr[i]) 
        }
    }
   return quickSort(left).concat([pivot], quickSort(right));
}

console.log(quickSort(arrayTest))
// 时间复杂度：O(nLogn)
