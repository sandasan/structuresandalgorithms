
// options1
/*

 [1  3  -1] -3  5  3  6  7       3
 1 [3  -1  -3] 5  3  6  7       3
 1  3 [-1  -3  5] 3  6  7       5
 1  3  -1 [-3  5  3] 6  7       5
 1  3  -1  -3 [5  3  6] 7       6
 1  3  -1  -3  5 [3  6  7]      7

*/
var maxSidlingWindow = function (nums, k) {
    if (k === 0 || nums.length === 0) return [];
    const deque = []; // 初始化双端队列deque
    const res = []; // 结果数组res
    // i = 1 - 3 = -2
    for(let j = 0, i = 1 - k; j < nums.length; j++,  i++) {
      
      // 当i>0时，说明已经有窗口元素已经满了，可以开始滑动并删减元素
      if (i > 0 && deque[0] === nums[i-1]) {
         // 窗口元素满了把第一个元素弹出。
        deque.shift();
      }
      console.log('j i', j, i, deque)
      // const nums = [1, 3, -1],
      // 要保持队列单调递减，判断队列末尾元素与下一个nums元素大小，队尾小的则要被淘汰
      while(deque && deque[deque.length - 1 ] < nums[j]) {
        // 弹出队尾元素
        deque.pop()
      }
      // 将新元素放入队列末尾
      deque.push(nums[j])

      if (i >= 0) {
        // 队首元素是每个窗口的最大值，存进res数组
         res[i] = deque[0]
      } 
    }
    return res;

}

const nums = [1, 3, -1],
 k = 3
console.log(maxSidlingWindow(nums, k)) 