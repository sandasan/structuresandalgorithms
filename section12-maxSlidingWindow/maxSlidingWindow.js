
// options1
/*

 [1  3  -1] -3  5  3  6  7       3
 1 [3  -1  -3] 5  3  6  7       3
 1  3 [-1  -3  5] 3  6  7       5
 1  3  -1 [-3  5  3] 6  7       5
 1  3  -1  -3 [5  3  6] 7       6
 1  3  -1  -3  5 [3  6  7]      7

*/
var maxSidlingWindow = function (nums, k) {
    if (k <= 1) return nums
    const res = [];
    // nums.length - k + 1

    // 需要循环 6 次
    for (let i = 0; i < nums.length - k + 1;  i ++ ) {
        // i = 0 (0, 3)
        // i = 1 (1, 4)
        // i = 2 (2, 5)
        // i = 3 (3, 6)
        // i = 4 (4, 7)
        // i = 5 (5, 8)
        console.log(i,  i + k)
        // slice(start, end) 包含start，不包含end
        res.push(Math.max(...nums.slice(i , i + k)))
    }
    return res
}

const nums = [1, 3, -1, -3, 5, 3, 6, 7],
 k = 3
console.log(maxSidlingWindow(nums, k)) 