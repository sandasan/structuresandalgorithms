class UnionFind {
    constructor(n) {
        this.parent = [];
        this.count = 0;
        this.init(n)
    }
    // 初始化一个大小是n的并查集
    init(n) {
        this.parent.length = 0;
        for(let i = 0; i < n; i++) {
            this.parent[i] = i
        }
        this.count = n
    }
    // 在并查集中找的节点node的分组
    find(node) {
      // O(1)
       return this.parent[node]
    }
    // 判定2个节点是不是联通的
    same(left, right) {}
    // 将2个节点连接起来
    union(left, right) {
        let index_l = this.find(left);
        let index_r = this.find(right);
        if (index_l !== index_r) {
          for (let i =0; i < this.parent.length; i++) {
            // 将所有的index_l分组的节点修改成index_r
            if (this.parent[i] == index_l){
                this.parent[i] = index_r
            }
            this.count--;
            return true
          }
          return false
        }
    }
    // 获取一共多少分组
    getCount() {}

} 

/*
上面的union()方法每次调用都会遍历整个并查集，找到需要修改的节点并修改节点所属的分组。
因此假如当新增的路径数目为m，并查集大小为n时，那么时间复杂度为O(m*n)。
当数据规模较大时平方阶的复杂度是不太理想的，因此我们需要探索更加高效的union()方法。
*/

// 用来解决不同节点是否有相同的根的问题
class UnionFind {
    constructor(n) {
        this.parent = []; // 并查集
        this.size = []; // 每一个节点下面有总节点的数目
        this.count = 0;
        this.init(n)
    }
    // 初始化一个大小是n的并查集
    init(n) {
        this.parent.length = 0;

        for(let i = 0; i < n; i++) {
            this.parent[i] = i
        }
        this.count = n
        this.size = new Array(n).fill(1)
    }
   // 在并查集中找到节点node的根节点
    find(node) {
      // O(1)
      while(node != this.parent[node]) {
        // 路径压缩，每次查找时都将子节点的父节点设置为父节点的父节点。
        // 这样能够不停的扁平化查询树。
         this.parent[node] = this.parent[this.parent[node]]
         node = this.parent[node]
      }
      return node;
    }
    // 判定2个节点是不是联通的
    // 判断两个节点的根节点是不是同一个
    same(left, right) {
      return this.find(left) === this.find(right)
    }
    // 将2个节点连接起来
    union(left, right) {
        let l = this.find(left);
        let r = this.find(right);
        if(l !== r) {
            //若左边较小将左边合并到右边的树上
            if (this.size(l) < this.size(r)) {
             this.parent[l] = r;
             this.size(r) += this.size(l)
            } else {
             this.parent[r] = l;
             this.size(l) += this.size(r)
            }
            this.count --;
            return true
        }
        return false
    }
    // 获取一共多少分组
    getCount() {
        this.count;
    }
}