var maxProduct = function (nums) {
    let max = nums[0]; // 存储子数组的最大乘积，默认为第一项
    // 由于乘积可能会出现负负得正的情况，因此需要同时存储最大和最小值
    // 这样能避免只存储最大值，导致负值被抛弃的情况
    // 但实际上minProducts和maxProducts都可能存在负值
    let minProducts = [max]; // 使用数组存储已计算出的最小值
    let maxProducts = [max]; // 使用数组存储已计算出的最大值
  
    // 遍历数组，计算最大乘积
    // 遍历数组，计算最大乘积
    for (let i = 1; i < nums.length; i++) {
      // 对于nums[i]来说，它一定会被计算到乘积中，但它之前已知的乘积是可以被抛弃的
      // 假设上一步的最大值和最小值需要使用，分别计算当前的最大和最小乘积
      const currMin = minProducts[i - 1] * nums[i];
      const currMax = maxProducts[i - 1] * nums[i];
      // 将currMin和currMax与当前值对比，找出i位置的真正最大和最小值，存入数组
      minProducts[i] = Math.min(currMin, currMax, nums[i]);
      maxProducts[i] = Math.max(currMin, currMax, nums[i]);
      // 不断对比当前最大值，即可找出整个数组中的最大乘积
      max = Math.max(max, maxProducts[i]);
    }
  
    return max;
  }

console.log(maxProduct([2, 3, -2, 4]))