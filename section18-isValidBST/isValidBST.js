function Node(val) {
    this.val = val;
    this.left = null;
    this.right =null;
}
/*

             6(a)
    4(b)               8  (c) 
                    7(f)  2(g)
    
             6
       4            8 
               7         3        
  
*/
var a = new Node(6);
var b = new Node(4);
var c = new Node(8);
var d = new Node(3);
var e = new Node(4);
var f = new Node(7);
var g = new Node(2);
var h = new Node(12);
a.left  = b
a.right = c

//c.left = f;
//c.right = d;

  //  节点的左子树只包含 小于 当前节点的数。
  //  节点的右子树只包含 大于 当前节点的数。
  // 所有左子树和右子树自身必须也是二叉搜索树。
 //            6 
 //     
 //      4             8
 //  2       1     45     21
  // 遍历左孩子得到最大值   max = helper(node.left)  --->(4)
  // 遍历右孩子得到最小值   min = helper(node.right) --->(8)
  // 然后判定 
  // 左边的最大值要小于根节点  （根节点要大于左边最大值）  max < root  4 < 6 
  // 右边的最小值要大于根节点   根节点要小于右边最小值  min > root  8 > 6
  // 同理递归
//   遍历左孩子得到最大值 max 
//   遍历右孩子得到最小值 min 
var isValidBST = function(root) {
 //                    6
 //              4            8 
 //                   
    // 判断是否在界限内部
    // 遍历左孩子得到最大值   max = helper(node.left)  --->(4)
    // 遍历右孩子得到最小值   min = helper(node.right) --->(8)
    // lower 下界  upper 上界
     function isValidBSTCore(root, rightTree, leftTree) {
        console.log(lower, upper)
         // 如果是null，则返回 true
         if(root === null) {
             return true;
         }
         // 左孩子得到最大值
         if(leftTree !== null && root.val >= leftTree) {
            return false
        }
         // 右孩子得到最小值
         if(rightTree !== null && root.val <= rightTree) {
             return false
         }
         
         // 对子树进行递归
         // 在递归调用左子树时，我们需要把上界 upper 改为 root.val，
         // 即调用 isValidBSTCore(root.left, lower, root.val)，因为左子树里所有节点的值均小于它的根节点的值。
         // 同理递归调用右子树时，我们需要把下界 lower 改为 root.val，即调用  isValidBSTCore(root.right, root.val, upper)
         return isValidBSTCore(root.left, lower, root.val)  &&  isValidBSTCore(root.right, root.val, upper);
     }
     return isValidBSTCore(root, null, null)
 };
console.log(isValidBST(a));
