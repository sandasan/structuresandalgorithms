function Node(val) {
    this.val = val;
    this.left = null;
    this.right = null;
}

const a = new Node(1);
const b = new Node(1);
const c = new Node(16);
const d = new Node(11);
const e = new Node(17);
const f = new Node(21);
const g = new Node(31);
a.left  = b
a.right  = c

c.left = f
c.right = g
//console.log(a);
/*
      1(a)

  1(b)       16 (c)
          21     31
  
  
*/
var lowestCommonAncestor = function(root, p, q) {
    // 若 root等于 p 或者 q的情况下则root是最近的祖先节点
    if (root == null || root == p || root == q) return root;
    // 若不是则
    const left = lowestCommonAncestor(root.left, p, q)
    const right = lowestCommonAncestor(root.right, p, q)
    if (left && right) {
        return root;
    }
    return left || right;

};
const arr = [3,5,1,6,2,0,8,null,null,7,4]
/*
              3
          5             1
     6      2        0      8
         7    4
*/

const p = 5,  q =  1
console.log(lowestCommonAncestor(arr, 5, 1))