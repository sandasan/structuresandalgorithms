function Node(key) {
    this.key = key;
    this.children = [];
}
   /*
           A
     B     C      D  
  E     F    
   */
let nA = new Node('A'),
nB = new Node('B')
nC = new Node('C'),
nD = new Node("D"),
nE = new Node("E")
nF = new Node("F")
nH = new Node("H")
nI = new Node("I")
nJ = new Node("J")
nK = new Node("K")
nG = new Node("G")
nL = new Node("L")

nA.children.push(nB)
nA.children.push(nC)
nA.children.push(nD)
const animals = [];

animals.push('b', 'c', 'd');
//console.log(animals);
/*
|     |
|     |
|     |
|  B  |
|  C  |
|  D  |
*/
nB.children.push(nE,nF)

//nC.children.push(nH)
//nD.children.push(nI, nJ)
//nE.children.push(nK)
//nH.children.push(nG)
//nJ.children.push(nL)
/*
|     |
|     |
|     |
|  B  |
|  C  |
|  D  |
*/
/*
|      |
|      |
|  E   |
|  F   |
|  C   |
|  D   |
*/
/*
|      |
|      |
|  K   |
|  F   |
|  C   |
|  D   |
*/
/*
|      |
|      |
|      |
|  F   |
|  C   |
|  D   |
*/
/*
|      |
|      |
|      |
|      |
|  C   |
|  D   |
*/
/*
|      |
|      |
|      |
|      |
|  H   |
|  D   |
*/
/*
|      |
|      |
|      |
|      |
|  G   |
|  D   |
*/
// 递归
function dfs(node) {
    node.children.forEach(dfs);
}

// 深度优先搜索算法实现
// 假设有一个[ ], 我们把 B,C,D 通过 unshift()放入[ D, C, B ],
// 通过shift()取出为 3, 2, 1 ,模拟栈的特性.
 /*
           A
     B     C      D  
  E     F    
   */
const depth2 = (node) => {
    let stack = []
    let nodes = []
    if (node) {
        stack.push(node)

        while (stack.length) {
        	//每次取最后一个
            console.log(stack)
            // stack --> [D, C, B]
            // 每一次取栈底的元素 取最后一个是 B 所以是 A, B
            // stack --> [F, E]
            // 每一次取栈底的元素 取最后一个是 E 所以是 A, B, E
            // 因为E没有子孩子, 所以跳出for stack --> [F]
            // 每一次取栈底的元素 取最后一个是 E 所以是 A, B, E, F

            // 因为F没有子孩子, 所以跳出for stack --> []
            // 每一次取栈底的元素 取最后一个是 E 所以是 A, B, E, F
            // 此时B 子孩子检测完毕。

            // 此时 stack --> [D, C]
            // 每一次取栈底的元素 取最后一个是 C 所以是 A, B, E, F, C
            // 发现C没有子孩子，出栈

            // 此时 stack --> [D]
            // 每一次取栈底的元素 取最后一个是 D 所以是 A, B, E, F, C, D
            // 发现D没有子孩子，出栈.全部完毕
            let item = stack.pop()
            
            let children = item.children || []
            nodes.push(item)
            //判断children的长度

             // 从孩子节点, 每一层的最后边进栈
             // -->stack.push(children[2]) -> D
             // -->stack.push(children[1]) -> C
             // -->stack.push(children[0]) -> B
             // 发现 B 有子节点F E （最后边进栈）
            for (let i = children.length - 1; i >= 0; i--) {
                // stack --> [F， E]
                stack.push(children[i])
            }
        }
    }
    return nodes
}

const res = depth2(nA)
console.log('res-',res)
// A B E F C D
