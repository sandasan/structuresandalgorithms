 var climbingStairs = function(n) {
    let arr = []   
    if (n === 1) {
        return 1;
    }
    if (n === 2) {
        return 2;
    }
    // 初始化数组的第一第二个元素
    arr[0] = 1;
    arr[1] = 2;
    if (n >= 3) {
    // 当n大于等于3的情形下递归
     for (let i = 2; i <=n ; i++) {
        arr[i] = arr[i - 1] + arr[ i - 2]
     }
    }
    return arr[n - 1]
   
 }

 console.log(climbingStairs(9))