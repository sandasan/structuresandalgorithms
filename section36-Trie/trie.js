class Trie {
    constructor () {
      this.data = {}     
    }
    //编写insert方法,通过isEnd属性标记一下，是不是一个完整的单词，方便后续的查询
    insert(word) {
        let node = this.data;
        for (let c of word) {
           
            if(node[c]) {
                node = node[c] 
            } else {
                node[c] = {}
                node = node[c]
            }
        }
        node['isEnd'] = true;
    }
    // 编写search方法，查找trie里是否存在一个完整的单词，通过isEnd属性判断
    search(word) {
        let node = this.data;
        for (let c of word) { 
            if (!node[c]) {
                return false;
            }  
            node = node[c]; 
        }
        return !!node['isEnd']
    }
    // 编写startWith 方法，查找trie中是否存在word的前缀,匹配则返回匹配的节点，方便后续的查找。
    startWith(word) {
        let node = this.data;
        for (let c of word) { 
            if (!node[c]) {
                return false;
            }  
            node = node[c]; 
        }
        return node
    }
    findSuffix(word) {
        // prefix app 共有前缀
        // node apple  {a: {p: {p: {l: {e: {isEnd: true }}}}}} 每一次递归遍历的对象
        // arr['app']  存放的结果数组
        function findAll(prefix, node, arr = [prefix]) {
            for(let key in node ) {
                console.log(key)// val
                if(key !== 'isEnd') {
                    let commonStr = prefix + key;
                    arr.push(commonStr)
                    findAll(commonStr, node[key], arr);
                }  
            } 
            return arr
        }
        let findingNode = this.startWith(word)
        console.log('findingNode', findingNode)
        if (!findingNode) { return false }  
        return findAll(word, findingNode)
    }
}

let trie = new Trie();
trie.insert('qpple')
trie.insert('appc')
console.log(trie.findSuffix('ap'));





